# In-Canopy Sensor Boards and Receiver Board

This repository contains the code for the three ESP32-based microcontrollers running in the Guided Recovery system developed by Project PHOENIX. These microcontrollers are responsible for sampling and transmitting the sensor data obtained from the parachute's canopy.

The code for both the receiver board and the in-canopy sensor boards (ICSBs) is collected in this repository because they share a sizeable amount of code.

**Also see the README.md** in the two sub-directories!

## Hardware

Both the in-canopy sensor board (ICSB) and the receiver board are an ESP32-based dual-core microcontrollers. The exact build is the [HUZZAH32](https://www.adafruit.com/product/3405) "Feather" board from AdaFruit, which features a LiPo adapter to plug the boards right into the battery.

### Sensors

Only the ICSB is connected to sensors. They are:

* BNO-055: accelerometer, gyroscope and magnetometer combined with an integrated microcontroller. This SiP already performs sensor fusion on these data to compute the linear acceleration, angular velocity and absolute orientation.
* BMP-388: absolute pressure and temperature sensor

## Software Architecture

The project is based on the ESP-IDF toolchain, featuring the [build system developed by Espressif](https://github.com/espressif/esp-idf). They include drivers for the peripherals as well as a FreeRTOS port on which we built our system. To build and run the project, it is first required to download the toolchain yourself, please follow the instructions in "[How to build this project](#How-to-build-this-project)".

### Components

ESP-IDF uses a CMake-based build systems that allows splitting up the code into separate "components". These represent small units of functionality that are separated as best as possible. There are some components that are shared, while some are only for one board.

#### Shared Components

| Module Name             | What does it do?                                             | Ready  for Review? | Reviewed / unchanged? |
| ----------------------- | ------------------------------------------------------------ | ------------------ | --------------------- |
| `data`                  | Definitions for sensor data structs – which data is being passed around in which format? | ✅                  |                       |
| `debug`                 | Useful debugging functionality, i. e. `printf()` statements that can be optimized away on building Release. Functions for timing code execution times (also only used during debugging). | ✅                  |                       |
| `packets`               | An abstraction layer for sending packets between microcontrollers. Allows sending different packet types through an arbitrary transmission layer (WiFi, SPI, …) and different priorities. Queue-based and works with RTOS. | ✅                  |                       |
| `i2c-bus`               | Peripheral driver for setting up the I2C bus (previously, the receiver board also had an I2C bus, that's why it is in the shared one) | ✅                  |                       |
| `wireless-transmission` | Wireless transmission layer for packets between the ESP-boards. | ✅                  |                       |
| `bmp388`                | Driver for the BMP-388 absolute pressure and temperature sensor. | ✅                  |                       |
| `bno055`                | Driver for the BNO-055 9-axis absolute orientation sensor. **Missing: **Pushing previous calibration values to the sensors | ✅                  |                       |

#### Separate components

The main modules and the peripherals are split up between the boards, because they vary. Also, the finite state machine is only active on the ICSB, because the receiver board doesn't have distinct states, it primarily acts as a relay.

## How to build this project

### Installation instructions

1. Install all the prerequisites by following the [guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#get-started-get-prerequisites) by Espressif. Prerequisites are cross-compilers, OpenOCD for debugging, CMake, and-so-forth.
2. Get the ESP-IDF by cloning the [git repository](https://github.com/espressif/esp-idf) and running the install scripts. Again, follow the [guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#get-started-get-esp-idf) there.
3. Clone this repository to a directory of your choosing.

### Building and flashing (Terminal based)

1. Before every build, navigate to the `esp-idf` folder and then run (Unix):

```
. ./export.sh
```

2. Navigate to the project folder and run `idf.py set-target esp32` to generate all the required build folders. Note that they will *not* be tracked by version control, because it contains paths from your local machine.
3. Run `idf.py build` to generate all the build files.
4. Connect the ESP32. Find its serial port.
5. Run `idf.py -p [PORT] flash` to flash the software.

### Building and flashing (VS Code extension)

One can also install the [VS Code extension](https://github.com/espressif/vscode-esp-idf-extension) by Espressif. This makes building, flashing and monitoring easier.