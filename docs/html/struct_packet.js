var struct_packet =
[
    [ "destination", "struct_packet.html#a2778d553074bf1d091cb006dac3618c8", null ],
    [ "origin", "struct_packet.html#a8d82ed701286851cd5883cbde29e70f6", null ],
    [ "payload", "struct_packet.html#ab34445d9d43f046ce471abf1ba0c03cb", null ],
    [ "payload_size", "struct_packet.html#a33127e81ce32d19843296372d0f8cba4", null ],
    [ "priority", "struct_packet.html#ab499c87b9436ff6a1946b8c00ba296aa", null ],
    [ "type", "struct_packet.html#a82667290d6f1f07c8fe44b5482d71da5", null ]
];