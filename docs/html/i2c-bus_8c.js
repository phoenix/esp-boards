var i2c_bus_8c =
[
    [ "ACK_CHECK_DIS", "i2c-bus_8c.html#a8ad7ceb373ce664b1c2482a66537e7c0", null ],
    [ "ACK_CHECK_EN", "i2c-bus_8c.html#a7dd22b838d2b1f22d367e6cdef04ccc3", null ],
    [ "ACK_VAL", "i2c-bus_8c.html#a1d64a546dacc15052a6c4887f9d0020f", null ],
    [ "I2C_FAST_MODE", "i2c-bus_8c.html#a1563098b2b4bb41e29f0e2d2e187b3c8", null ],
    [ "I2C_MASTER_RX_BUF_DISABLE", "i2c-bus_8c.html#a37a0707200e50e3b3e9ab28b1b8d6777", null ],
    [ "I2C_MASTER_TX_BUF_DISABLE", "i2c-bus_8c.html#aaa0e84f340ef5ea9db2d7624fdadaa26", null ],
    [ "I2C_STANDARD_MODE", "i2c-bus_8c.html#aea01e023ea5246883d088908e0fc11d2", null ],
    [ "LAST_NACK", "i2c-bus_8c.html#a000e4b23ae626497ff209202a75520fe", null ],
    [ "NACK_VAL", "i2c-bus_8c.html#ae20bf16f65c41237e2d620ef49890781", null ],
    [ "READ_BIT", "i2c-bus_8c.html#a2f493ed233e66342493f155ebda5c183", null ],
    [ "WRITE_BIT", "i2c-bus_8c.html#a7fc57d5be9f588839a00c75ef2946e17", null ],
    [ "i2c_bus_setup", "i2c-bus_8c.html#a3bd802abe38df0fccf2980d047d3aa2a", null ],
    [ "i2c_master_read_slave_reg", "i2c-bus_8c.html#a87ddf2c8b22ff9aca34f3675f7ebb5e5", null ],
    [ "i2c_master_write_slave_reg", "i2c-bus_8c.html#ae2258a2257e18428ba75e551d6f66faf", null ],
    [ "i2c_test_device_connection", "i2c-bus_8c.html#ad249ee74743542f5bcaba7e83d305267", null ]
];