var packets_8h =
[
    [ "Packet", "struct_packet.html", "struct_packet" ],
    [ "CommandPayload", "struct_command_payload.html", "struct_command_payload" ],
    [ "DataPayload", "struct_data_payload.html", "struct_data_payload" ],
    [ "PacketTransmissionHandler", "struct_packet_transmission_handler.html", "struct_packet_transmission_handler" ],
    [ "PacketTaskConfig", "struct_packet_task_config.html", "struct_packet_task_config" ],
    [ "Command", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153", [
      [ "CommandDummy", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153a456823c33c1e552706ee029d3200bd5b", null ],
      [ "CommandActivate", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153ab0c26d6eb0a279fdeca7a450430a87a5", null ],
      [ "CommandSleep", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153ae59814ec08c5198cd7be6b876c622495", null ],
      [ "CommandCalibrate", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153a68e118c926d5f60a499c7f8808568c5e", null ],
      [ "CommandRestart", "packets_8h.html#a2afce0a47a93eee73a314d53e4890153af92d0acb21743477510af9158c654652", null ]
    ] ],
    [ "Location", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43", [
      [ "ReceiverBoard", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43a9e3ddb3e00f74fa4a7c9bfd3913d77fa", null ],
      [ "InCanopyBoardLeft", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43a60fffa6de0067446dfd5aae6824de8fc", null ],
      [ "InCanopyBoardRight", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43ae0814c333c065aca993fcfef33b5d824", null ],
      [ "MainControllerBoard", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43aebcf30530271c4c62a2a038811acc2c9", null ],
      [ "WirelessRemoteBoard", "packets_8h.html#aecaf6a9545fa815deb4b8e64e144ce43a9f928faf8eac8e43e2524f8d70dacfe6", null ]
    ] ],
    [ "PacketType", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0", [
      [ "Empty", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0a891fbec6fb0d74aea9e064d13b59c9e0", null ],
      [ "LogPacket", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0a8996bc88c0318e202343afbc5b9f991f", null ],
      [ "DataPacket", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0ae1af50ee7168a919cdb7d7832871e309", null ],
      [ "CommandPacket", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0a6199fbf43c8a76d9c830b9373bcc768c", null ],
      [ "StatePacket", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0a7c331d5e5e0702c6baeaffb6fad1f695", null ],
      [ "MultiPacket", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0aaa2bb52cf34476f312a230f9b01e5bb3", null ],
      [ "KeepAlive", "packets_8h.html#a0a80a7bc045affcf10846075b88cbca0af5541a2588b659dabb45784ac06793b2", null ]
    ] ],
    [ "PriorityLevel", "packets_8h.html#a58ef2aaf285eac5903ead43e801a4513", [
      [ "PriorityNormal", "packets_8h.html#a58ef2aaf285eac5903ead43e801a4513a0b38c2d97637855b60c385a5c2f7456f", null ],
      [ "PriorityHigh", "packets_8h.html#a58ef2aaf285eac5903ead43e801a4513a0f3884d813132961198d9caa7149d539", null ]
    ] ],
    [ "packet_add_to_queue", "packets_8h.html#a2d6bf6ef98e3e337bafda326d36231ac", null ],
    [ "packet_task_entry", "packets_8h.html#a1d9e99c9d401443a65c6393ab7d1bfed", null ]
];