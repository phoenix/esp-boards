var sensors_8c =
[
    [ "NUM_SAMPLING_ERRORS_BEFORE_RESET", "sensors_8c.html#aff8d9e8007d8baccb86d62adac975d23", null ],
    [ "sensors_calibrate", "sensors_8c.html#a951803b555b9edc604ea4ddc17ae6a36", null ],
    [ "sensors_enable_lowpower", "sensors_8c.html#a92eee546d56b2656ba4f551205eae573", null ],
    [ "sensors_start_sampling", "sensors_8c.html#aaf961753f6d2ab4ad2bb454bbbdb076a", null ],
    [ "sensors_stop_sampling", "sensors_8c.html#a925fbd3048957a609bbea393f9fd5d33", null ],
    [ "sensors_task_entry", "sensors_8c.html#ae0bb3a70e9f45db283606e2088711563", null ],
    [ "sensors_wakeup", "sensors_8c.html#a7ec9fc8bb96036774d8a5f8adb9c1a7a", null ],
    [ "timer_callback", "sensors_8c.html#a0d30a6345e916f08e4c49b0a9f804606", null ],
    [ "bmp_388", "sensors_8c.html#a792436bfd237a3d1a02e0209e4079570", null ],
    [ "bno_055", "sensors_8c.html#a872aea81041ec83ceb7ceb5229ffc763", null ]
];