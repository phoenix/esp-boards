var bno_055_8h =
[
    [ "BNO055_Struct", "struct_b_n_o055___struct.html", "struct_b_n_o055___struct" ],
    [ "BNO055_Handle", "bno-055_8h.html#a154c9265fea112b65d2d6ddb92609e88", null ],
    [ "AccelFullscaleRange", "bno-055_8h.html#a3e58d5e2df17e9f35d8826d2ea08972a", [
      [ "ACCEL_FSR2G", "bno-055_8h.html#a3e58d5e2df17e9f35d8826d2ea08972aa9655d943d8550b67251401dc359d73dd", null ],
      [ "ACCEL_FSR4G", "bno-055_8h.html#a3e58d5e2df17e9f35d8826d2ea08972aaf50801e1f72c793f566b11b86c99835a", null ],
      [ "ACCEL_FSR8G", "bno-055_8h.html#a3e58d5e2df17e9f35d8826d2ea08972aa17d5321488f779bcea386d5e1e48669a", null ],
      [ "ACCEL_FSR16G", "bno-055_8h.html#a3e58d5e2df17e9f35d8826d2ea08972aa206c9b51333e6d0cf8c5d0901414ee18", null ]
    ] ],
    [ "GyroFullscaleRange", "bno-055_8h.html#abf3bdd237936e004be319913f060f155", [
      [ "GYRO_FSR2000", "bno-055_8h.html#abf3bdd237936e004be319913f060f155ababc835568ad2e6471c3a6bdc41d3bc5", null ],
      [ "GYRO_FSR1000", "bno-055_8h.html#abf3bdd237936e004be319913f060f155ae00fa6c78d7ce0dd88b17cd015198637", null ],
      [ "GYRO_FSR500", "bno-055_8h.html#abf3bdd237936e004be319913f060f155ade65f2e35a80fcb98053655b3fe1a8af", null ],
      [ "GYRO_FSR250", "bno-055_8h.html#abf3bdd237936e004be319913f060f155aa020f99dea251b140034600157701964", null ],
      [ "GYRO_FSR125", "bno-055_8h.html#abf3bdd237936e004be319913f060f155a98380caf49bd6c89c453339ea14bcf11", null ]
    ] ],
    [ "bno055_is_calibrated", "bno-055_8h.html#a9198f1b90606409b3fe27f3b5d989926", null ],
    [ "bno055_lowpower_enable", "bno-055_8h.html#a23063f4cb9a7c4d94b5ccde348efbddc", null ],
    [ "bno055_print_calibration", "bno-055_8h.html#aa95fda186d56dad0f1b2a20a5854bc01", null ],
    [ "bno055_push_calibration", "bno-055_8h.html#a067e5c8eb822cf99e427b97af2c5fb6c", null ],
    [ "bno055_reset", "bno-055_8h.html#ac6d4bda9b9fb6a98922f994f29d3c32d", null ],
    [ "bno055_sample", "bno-055_8h.html#a74682bbb4f9708b2b2d04c4bee378a1a", null ],
    [ "bno055_setup", "bno-055_8h.html#af96524ed85ce9cabe5746fb541bdb103", null ],
    [ "bno055_wakeup", "bno-055_8h.html#a07157932158227180828825473d13946", null ]
];