var searchData=
[
  ['sensor_5ftype_2343',['sensor_type',['../struct_data_payload.html#ac16e2d5b12ffea5b3400098c5df1915a',1,'DataPayload']]],
  ['setup_5fevent_5fgroup_2344',['setup_event_group',['../receiver-board_2main_2include_2main_8h.html#a8c8202bee91c805730e58ac2663d3f76',1,'setup_event_group():&#160;main.c'],['../receiver-board_2main_2main_8c.html#a8c8202bee91c805730e58ac2663d3f76',1,'setup_event_group():&#160;main.c']]],
  ['sic_5f0_2345',['sic_0',['../structbno055__sic__matrix__t.html#a7f65d73ea4dee5bfcc83b5186d4dc007',1,'bno055_sic_matrix_t']]],
  ['sic_5f1_2346',['sic_1',['../structbno055__sic__matrix__t.html#a10eefababe193767518a8ab3ac801ca5',1,'bno055_sic_matrix_t']]],
  ['sic_5f2_2347',['sic_2',['../structbno055__sic__matrix__t.html#a55d2628b3c699d5e2c2abf2e64ecc78d',1,'bno055_sic_matrix_t']]],
  ['sic_5f3_2348',['sic_3',['../structbno055__sic__matrix__t.html#ac516b74a2858ea9fe753331b99b55d3b',1,'bno055_sic_matrix_t']]],
  ['sic_5f4_2349',['sic_4',['../structbno055__sic__matrix__t.html#af4d80149aebcf8348be08a9ac34e2944',1,'bno055_sic_matrix_t']]],
  ['sic_5f5_2350',['sic_5',['../structbno055__sic__matrix__t.html#a942a97b215f16a9505d701b06c1abd88',1,'bno055_sic_matrix_t']]],
  ['sic_5f6_2351',['sic_6',['../structbno055__sic__matrix__t.html#a752f63b0b4bb75e88e0e1d612998b92a',1,'bno055_sic_matrix_t']]],
  ['sic_5f7_2352',['sic_7',['../structbno055__sic__matrix__t.html#afbb8bfd1ba21b362620cf7178e89b1f5',1,'bno055_sic_matrix_t']]],
  ['sic_5f8_2353',['sic_8',['../structbno055__sic__matrix__t.html#aa74051638e5f251cf9fd40b7ae22a28c',1,'bno055_sic_matrix_t']]],
  ['spi_5fevents_2354',['spi_events',['../spi-transmission_8c.html#a7ff935d0af10ea14e236e3e0ef6e457d',1,'spi-transmission.c']]],
  ['sw_5frev_5fid_2355',['sw_rev_id',['../structbno055__t.html#a92e6f4f6bcd30557f41ac4674fb3eccb',1,'bno055_t']]]
];
