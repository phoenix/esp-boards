var searchData=
[
  ['take_5fpressure_5fmeasurement_2253',['take_pressure_measurement',['../calibratometer_2main_2include_2main_8h.html#ae672142e388966c1c554c63235a59e15',1,'take_pressure_measurement(int argc, char **argv):&#160;main.c'],['../calibratometer_2main_2main_8c.html#ae672142e388966c1c554c63235a59e15',1,'take_pressure_measurement(int argc, char **argv):&#160;main.c']]],
  ['timer_5fcallback_2254',['timer_callback',['../sensors_8c.html#a0d30a6345e916f08e4c49b0a9f804606',1,'sensors.c']]],
  ['transmit_5fpacket_5fspi_2255',['transmit_packet_spi',['../spi-transmission_8h.html#a4d3984eefe6d5bb839ce34f771428835',1,'transmit_packet_spi(Packet *packet):&#160;spi-transmission.c'],['../spi-transmission_8c.html#a4d3984eefe6d5bb839ce34f771428835',1,'transmit_packet_spi(Packet *packet):&#160;spi-transmission.c']]],
  ['transmit_5fpacket_5fwirelessly_2256',['transmit_packet_wirelessly',['../wireless-transmission_8h.html#af964bbf112656816cb9a59a565cbd594',1,'transmit_packet_wirelessly(Packet *packet):&#160;wireless-transmission.c'],['../wireless-transmission_8c.html#af964bbf112656816cb9a59a565cbd594',1,'transmit_packet_wirelessly(Packet *packet):&#160;wireless-transmission.c']]]
];
