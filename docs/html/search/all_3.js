var searchData=
[
  ['data_2ec_1528',['data.c',['../data_8c.html',1,'']]],
  ['data_2eh_1529',['data.h',['../data_8h.html',1,'']]],
  ['datapacket_1530',['DataPacket',['../packets_8h.html#a0a80a7bc045affcf10846075b88cbca0ae1af50ee7168a919cdb7d7832871e309',1,'packets.h']]],
  ['datapayload_1531',['DataPayload',['../struct_data_payload.html',1,'']]],
  ['debug_1532',['DEBUG',['../debug_8h.html#ad72dbcf6d0153db1b8d8a58001feed83',1,'debug.h']]],
  ['debug_2ec_1533',['debug.c',['../debug_8c.html',1,'']]],
  ['debug_2eh_1534',['debug.h',['../debug_8h.html',1,'']]],
  ['debug_5floop_1535',['debug_loop',['../receiver-board_2main_2include_2main_8h.html#aafdb69592ffcc242d206e715a63750e1',1,'debug_loop(void):&#160;main.c'],['../receiver-board_2main_2main_8c.html#aafdb69592ffcc242d206e715a63750e1',1,'debug_loop(void):&#160;main.c']]],
  ['debug_5fprintf_1536',['debug_printf',['../debug_8h.html#ad4d0181076bcf51c58cf91540abac61e',1,'debug.h']]],
  ['default_5fvref_1537',['DEFAULT_VREF',['../battery_8c.html#a63baed8df076ce9dddaea1ae8694c1d8',1,'battery.c']]],
  ['delay_5fmsec_1538',['delay_msec',['../structbno055__t.html#ad5e7501aecd612631e786b4a5555282f',1,'bno055_t']]],
  ['deserialize_5fdata_5fpayload_1539',['deserialize_data_payload',['../serialization_8h.html#a7a151805af93afdfbf686bec2ad7c6c7',1,'deserialize_data_payload(uint8_t *buf, struct DataPayload *pl):&#160;serialization.c'],['../serialization_8c.html#a7a151805af93afdfbf686bec2ad7c6c7',1,'deserialize_data_payload(uint8_t *buf, struct DataPayload *pl):&#160;serialization.c']]],
  ['destination_1540',['destination',['../struct_packet.html#a2778d553074bf1d091cb006dac3618c8',1,'Packet::destination()'],['../struct_packet_transmission_handler.html#ab6206422025d48b96117c93dcf156041',1,'PacketTransmissionHandler::destination()']]],
  ['dests_1541',['dests',['../struct_wireless_task_configuration.html#a3dd152661702009cc1e01533446e07b8',1,'WirelessTaskConfiguration']]],
  ['dev_5faddr_1542',['dev_addr',['../structbno055__t.html#a52bbddba1521f190a7335896182ca33c',1,'bno055_t']]],
  ['dma_5fchan_1543',['DMA_CHAN',['../spi-transmission_8c.html#a5c8e512df4a72e57db32aca9123c172f',1,'spi-transmission.c']]],
  ['down_1544',['down',['../union_position.html#af1bfda628a82eb757cbe1767bbe379a5',1,'Position']]]
];
