var searchData=
[
  ['sclk_5fpin_3599',['SCLK_PIN',['../spi-transmission_8c.html#ac9bb6d868837c450a2172efa97326d4a',1,'spi-transmission.c']]],
  ['sensors_5fready_3600',['SENSORS_READY',['../receiver-board_2main_2include_2main_8h.html#a8d5b56f03e8df2265a0137e51c35395f',1,'main.h']]],
  ['sleep_5fduration_5fusec_3601',['SLEEP_DURATION_USEC',['../sleep_8c.html#a4daf3e6279c51bc9ae0020bc5af0e203',1,'sleep.c']]],
  ['spi_5fdevice_3602',['SPI_DEVICE',['../spi-transmission_8c.html#a7b3fe8993c50b10f70075ac9813f5a2a',1,'spi-transmission.c']]],
  ['spi_5ftrans_5fcpltd_3603',['SPI_TRANS_CPLTD',['../spi-transmission_8c.html#a89e6bf004076a2409d9c59bd4469f928',1,'spi-transmission.c']]],
  ['spi_5ftrans_5fsetup_3604',['SPI_TRANS_SETUP',['../spi-transmission_8c.html#ad4e5ff03926fff68e946770cdebd2d0a',1,'spi-transmission.c']]],
  ['spl_5fwichlen_3605',['SPL_WICHLEN',['../calibratometer_2main_2main_8c.html#a0861e1d1cae0c4471ec72a24e8cca7b6',1,'main.c']]]
];
