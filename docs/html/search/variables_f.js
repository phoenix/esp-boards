var searchData=
[
  ['r_2336',['r',['../structbno055__euler__t.html#ab337299f3ea60608118a074a9eef9997',1,'bno055_euler_t::r()'],['../structbno055__euler__double__t.html#a59aad76236b2f1eb91d3466f61fc72c2',1,'bno055_euler_double_t::r()'],['../structbno055__euler__float__t.html#abc54c08f7a1b0c4edd9db988fb3603c5',1,'bno055_euler_float_t::r()'],['../structbno055__accel__offset__t.html#a6ef6b60b88510901703c71820f0e763d',1,'bno055_accel_offset_t::r()'],['../structbno055__mag__offset__t.html#a29ed94bdcb683a23d96fa94a9e05286a',1,'bno055_mag_offset_t::r()'],['../union_raw_angular_velocity.html#a638046d9ee3567ce1ba32b5b8b47046d',1,'RawAngularVelocity::r()'],['../union_angular_velocity.html#a45daf6420d0b6efb14cb1cc60d70b6a8',1,'AngularVelocity::r()']]],
  ['readout_2337',['readout',['../calibratometer_2main_2main_8c.html#a0d7baf7b4c129a52271cade02cf42070',1,'main.c']]],
  ['reception_5fhandler_2338',['reception_handler',['../struct_packet_task_config.html#a4f8455b7b3cd9162ec1946f6add2d338',1,'PacketTaskConfig']]],
  ['reset_5fpin_2339',['reset_pin',['../struct_b_n_o055___struct.html#a920f9e61510ee222721cca5865a0668b',1,'BNO055_Struct']]],
  ['right_5fwoken_2340',['right_woken',['../calibratometer_2main_2main_8c.html#ac3ee9d6af4e04a89c2dea4796dabdfe4',1,'main.c']]],
  ['roll_2341',['roll',['../union_raw_heading.html#a98182575004421621d2073aef06a0fd4',1,'RawHeading::roll()'],['../union_heading.html#ac87f4266ba8e68760502a230c9b603cf',1,'Heading::roll()']]],
  ['rst_5fpin_5fsetup_2342',['rst_pin_setup',['../struct_b_n_o055___struct.html#a01bca0d89f663f06747110d57d52a600',1,'BNO055_Struct']]]
];
