var i2c_bus_8h =
[
    [ "func", "i2c-bus_8h.html#a28c25a7dc20282d3df5ca15ee7c714e9", null ],
    [ "i2c_bus_setup", "i2c-bus_8h.html#a3bd802abe38df0fccf2980d047d3aa2a", null ],
    [ "i2c_master_read_slave_reg", "i2c-bus_8h.html#a87ddf2c8b22ff9aca34f3675f7ebb5e5", null ],
    [ "i2c_master_write_slave_reg", "i2c-bus_8h.html#ae2258a2257e18428ba75e551d6f66faf", null ],
    [ "i2c_test_device_connection", "i2c-bus_8h.html#ad249ee74743542f5bcaba7e83d305267", null ]
];