var spi_transmission_8c =
[
    [ "CS_PIN", "spi-transmission_8c.html#abfcf05153ddbd63d5aff5d018867cc19", null ],
    [ "DMA_CHAN", "spi-transmission_8c.html#a5c8e512df4a72e57db32aca9123c172f", null ],
    [ "MISO_PIN", "spi-transmission_8c.html#aecb75580e6d96b71a64123aee5bd3929", null ],
    [ "MOSI_PIN", "spi-transmission_8c.html#a11338fccf824b29757c2b23edb0f690f", null ],
    [ "READY_PIN", "spi-transmission_8c.html#a74b40f847b0828e382397e5328fef511", null ],
    [ "SCLK_PIN", "spi-transmission_8c.html#ac9bb6d868837c450a2172efa97326d4a", null ],
    [ "SPI_DEVICE", "spi-transmission_8c.html#a7b3fe8993c50b10f70075ac9813f5a2a", null ],
    [ "SPI_TRANS_CPLTD", "spi-transmission_8c.html#a89e6bf004076a2409d9c59bd4469f928", null ],
    [ "SPI_TRANS_SETUP", "spi-transmission_8c.html#ad4e5ff03926fff68e946770cdebd2d0a", null ],
    [ "spi_interface_initialize", "spi-transmission_8c.html#a0f4904954dc396c3be7a421c95594a32", null ],
    [ "spi_interface_task", "spi-transmission_8c.html#add13391541b4425eb3036f3003d2fb83", null ],
    [ "transmit_packet_spi", "spi-transmission_8c.html#a4d3984eefe6d5bb839ce34f771428835", null ],
    [ "spi_events", "spi-transmission_8c.html#a7ff935d0af10ea14e236e3e0ef6e457d", null ]
];