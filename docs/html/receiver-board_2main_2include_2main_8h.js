var receiver_board_2main_2include_2main_8h =
[
    [ "PACKETS_API_READY", "receiver-board_2main_2include_2main_8h.html#a456b25697bc3c3e4b4e69e0b84389400", null ],
    [ "PRIORITY_MAIN_TASK", "receiver-board_2main_2include_2main_8h.html#ae53cee054f771fb3c1c8260b4bdb5034", null ],
    [ "PRIORITY_PCKT_TASK", "receiver-board_2main_2include_2main_8h.html#a589b385d18bd0feed6bc2ef4488b93dd", null ],
    [ "PRIORITY_SENS_TASK", "receiver-board_2main_2include_2main_8h.html#a7716c9fe253caa7e92451155418e9730", null ],
    [ "PRIORITY_SPIC_TASK", "receiver-board_2main_2include_2main_8h.html#a92650d0bb54c0b5c976b596fa6fa73ec", null ],
    [ "PRIORITY_WKUP_TASK", "receiver-board_2main_2include_2main_8h.html#ae735b98d264cc10956cdb0b13c6c7bb8", null ],
    [ "SENSORS_READY", "receiver-board_2main_2include_2main_8h.html#a8d5b56f03e8df2265a0137e51c35395f", null ],
    [ "WIFI_READY", "receiver-board_2main_2include_2main_8h.html#a63953541e9986bbd057c31e2b605729d", null ],
    [ "debug_loop", "receiver-board_2main_2include_2main_8h.html#aafdb69592ffcc242d206e715a63750e1", null ],
    [ "fatal_error", "receiver-board_2main_2include_2main_8h.html#a69d5ff687dbec547ad43927aed7e3381", null ],
    [ "packet_received", "receiver-board_2main_2include_2main_8h.html#ae892a842adb5536aa7ab5d7df9ac10db", null ],
    [ "standard_loop", "receiver-board_2main_2include_2main_8h.html#a86e707d84230117d2d68e40ad184c195", null ],
    [ "setup_event_group", "receiver-board_2main_2include_2main_8h.html#a8c8202bee91c805730e58ac2663d3f76", null ]
];