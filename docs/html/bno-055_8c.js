var bno_055_8c =
[
    [ "BNO055_MAX_ACCEL", "bno-055_8c.html#ae9d6c66f693eb2897a98dc55bb1b3ed2", null ],
    [ "BNO055_MAX_QUAT_COMP", "bno-055_8c.html#a03884550a9e2f6fed3954ef6c32bf529", null ],
    [ "BNO055_MAX_RPS", "bno-055_8c.html#a9759d246bd499d1814575b064a93d1db", null ],
    [ "BNO055_MIN_ACCEL", "bno-055_8c.html#afba32e8114cd77153f110b045c32209e", null ],
    [ "BNO055_MIN_QUAT_COMP", "bno-055_8c.html#a70f917890aac7cdfbf197ead009a0c98", null ],
    [ "BNO055_MIN_RPS", "bno-055_8c.html#aeeca7a484d96b1786fcaf91ff00971a7", null ],
    [ "bno055_delay", "bno-055_8c.html#ac22ca8c6c4b6a6d28c4d678e50e81ba1", null ],
    [ "bno055_is_calibrated", "bno-055_8c.html#a9198f1b90606409b3fe27f3b5d989926", null ],
    [ "bno055_lowpower_enable", "bno-055_8c.html#a23063f4cb9a7c4d94b5ccde348efbddc", null ],
    [ "bno055_print_calibration", "bno-055_8c.html#aa95fda186d56dad0f1b2a20a5854bc01", null ],
    [ "bno055_push_calibration", "bno-055_8c.html#a067e5c8eb822cf99e427b97af2c5fb6c", null ],
    [ "bno055_read", "bno-055_8c.html#ad22f8837af8df2fc43d27cbbbb2e5eb0", null ],
    [ "bno055_reset", "bno-055_8c.html#ac6d4bda9b9fb6a98922f994f29d3c32d", null ],
    [ "bno055_sample", "bno-055_8c.html#a74682bbb4f9708b2b2d04c4bee378a1a", null ],
    [ "bno055_setup", "bno-055_8c.html#af96524ed85ce9cabe5746fb541bdb103", null ],
    [ "bno055_wakeup", "bno-055_8c.html#a07157932158227180828825473d13946", null ],
    [ "bno055_write", "bno-055_8c.html#aeca5b924362f7d3ac33c54043dcf8493", null ]
];