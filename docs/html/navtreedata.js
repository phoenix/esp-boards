/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mainboard Project PHOENIX", "index.html", [
    [ "In-canopy sensor boards", "md_in_canopy_sensor_board__r_e_a_d_m_e.html", [
      [ "Overview", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md1", [
        [ "Hardware", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md2", null ]
      ] ],
      [ "Software Architecture", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md3", [
        [ "ICSB Modules", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md4", null ],
        [ "Tasks", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md5", [
          [ "Task overview", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md6", null ],
          [ "Main task", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md7", null ]
        ] ],
        [ "Finite State Machine", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md8", [
          [ "Sleep Mode", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md9", null ]
        ] ]
      ] ],
      [ "Reference Manual", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md10", [
        [ "Nominal behaviour", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md11", null ],
        [ "Remote Commands", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md12", null ],
        [ "Pinout", "md_in_canopy_sensor_board__r_e_a_d_m_e.html#autotoc_md13", null ]
      ] ]
    ] ],
    [ "Receiver Board", "md_receiver_board__r_e_a_d_m_e.html", [
      [ "Hardware", "md_receiver_board__r_e_a_d_m_e.html#autotoc_md15", null ],
      [ "Software Architecture", "md_receiver_board__r_e_a_d_m_e.html#autotoc_md16", [
        [ "Receiver Board Modules", "md_receiver_board__r_e_a_d_m_e.html#autotoc_md17", null ]
      ] ],
      [ "Reference Manual", "md_receiver_board__r_e_a_d_m_e.html#autotoc_md18", [
        [ "Pinout", "md_receiver_board__r_e_a_d_m_e.html#autotoc_md19", null ]
      ] ]
    ] ],
    [ "Calibratometer", "md_calibratometer__r_e_a_d_m_e.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"bno-055-bosch_8h.html#a008e6d6574f9006ffaf6a137794fc262",
"bno-055-bosch_8h.html#a30b116c3d0a5dcfc0bf89c5ef19ee060",
"bno-055-bosch_8h.html#a5b4a3fd1db53d480f2426c43ef6deee1",
"bno-055-bosch_8h.html#a8dbb52ea0cce616e5516dde42424cd3a",
"bno-055-bosch_8h.html#ab9eedcddd0d13a450b05defd9477e70d",
"bno-055-bosch_8h.html#ae2c6c25debb5bb6e1ad4a6733442b11b",
"dir_39751dc783b04b67d7d1b2e069d0c6f5.html",
"sensors_8c.html#a0d30a6345e916f08e4c49b0a9f804606",
"union_geodetic_position.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';