var struct_complete_sensor_data =
[
    [ "changed_readings", "struct_complete_sensor_data.html#ac3742f703377e5fd49be6d3c1c0e2be2", null ],
    [ "geodetic_pos_1", "struct_complete_sensor_data.html#a2bc737965ba65e824cf28f445c80af46", null ],
    [ "geodetic_pos_2", "struct_complete_sensor_data.html#a1e9e6debb0da5a8c8478b74db470a808", null ],
    [ "icsb_att_readout_1", "struct_complete_sensor_data.html#a3b5b3f5d4cc1670e62895d0b95ed9ea5", null ],
    [ "icsb_att_readout_2", "struct_complete_sensor_data.html#a6bce7dfe5110f9b3099dfbfd5d68886b", null ],
    [ "icsb_pressure_left", "struct_complete_sensor_data.html#a7e1df45cef7aac4ec4dee721cfec4235", null ],
    [ "icsb_pressure_right", "struct_complete_sensor_data.html#ae994f88cd282b9e2e085fa8b47e51a28", null ],
    [ "main_att_readout_1", "struct_complete_sensor_data.html#aafa03053c81f9eb73eb1b3b5eff1c03b", null ],
    [ "main_att_readout_2", "struct_complete_sensor_data.html#a603153decd205d72d68d6bb8e40ffb06", null ],
    [ "main_tube_pressure_1", "struct_complete_sensor_data.html#a47a3b167a6e849664d47f7696af3bf56", null ],
    [ "main_tube_pressure_2", "struct_complete_sensor_data.html#ad0aaf16551621dc09b81f0fc922d2a4d", null ]
];