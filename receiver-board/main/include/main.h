/**
 *******************************************************************************
 * @file           main.h
 * @brief          Global declarations for the entire application.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include "freertos/event_groups.h"

#include "packets.h"

#define PRIORITY_MAIN_TASK 3 ///< Priority of the main program body
#define PRIORITY_PCKT_TASK 5 ///< Priority of the packet task
#define PRIORITY_SENS_TASK 4 ///< Priority of the sensor polling task
#define PRIORITY_SPIC_TASK 3 ///< Priority of the SPI cleanup task
#define PRIORITY_WKUP_TASK 4 ///< Priority of the wakeup task.

/* Application flow --------------------------------------------------------- */

/**
 * @brief Event group storing the setup flags raised by setup functionality.
 * See below for the various flags that are raised by the respective setup
 * functionality in task's entry functions. For example, the sensors task will
 * raise a flag when sensor sampling is available. This mechanism is used to
 * avoid race conditions.
 */
extern EventGroupHandle_t setup_event_group;

#define PACKETS_API_READY (1 << 0)
#define WIFI_READY        (1 << 1)
#define SENSORS_READY     (1 << 2)

/* Operation modes ---------------------------------------------------------- */

/**
 * Enters the debug loop.
 *
 * This "debug loop" is the operation mode in which one can issue commands to
 * the ICSB via UART stdin.
 */
void debug_loop(void) __attribute__((noreturn));

/**
 * Enters the standard loop, in which the main task goes to sleep.
 */
void standard_loop(void) __attribute__((noreturn));

/* Error handling ----------------------------------------------------------- */

/**
 * Handler for when a fatal error occurred in the system. Delay forever.
 */
void fatal_error(void) __attribute__((noreturn));

/* Packet handling ---------------------------------------------------------- */

/**
 * Default packet reception handler.
 *
 * This default packet reception handler will toggle the LED when a packet has
 * been received. It will then discard the packet. This function is called from
 * the packet handling task.
 * @param packet The packet that was received.
 */
void packet_received(Packet *packet);

#endif /* __MAIN_H__ */
