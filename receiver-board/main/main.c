/**
 *******************************************************************************
 * @file           main.c
 * @brief          Main program body
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "driver/uart.h"
#include "esp_log.h"
#include "esp_vfs.h"
#include "esp_vfs_dev.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#include "debug.h"
#include "i2c-bus.h"
#include "icsb-wakeup.h"
#include "main.h"
#include "packets.h"
#include "peripherals.h"
#include "spi-transmission.h"
#include "wireless-transmission.h"

/* Local variables ---------------------------------------------------------- */
static TaskHandle_t packet_task;
static TaskHandle_t spi_task;
static TaskHandle_t wakeup_task;
static const char *TAG = "main";

/* Global RTOS variables, exported via main.h ------------------------------- */

EventGroupHandle_t setup_event_group;

/* Public functions --------------------------------------------------------- */

void app_main(void) {
    // Print message to serial monitor identifying the current build
    ESP_LOGI(TAG, "\n"
                  "========================================\n"
                  "   Receiver Board                       \n"
                  "       by Project PHOENIX of ARIS       \n"
#if DEBUG == 1
                  "          Debug build                   \n"
#else
                  "          Release build                 \n"
#endif /* DEBUG == 1 */
                  "========================================\n");

    /* Global RTOS setup ---------------------------------------------------- */

    // Set priority of main task (lower than packet task)
    vTaskPrioritySet(NULL, PRIORITY_MAIN_TASK);

    // Create event group used to determine setup progress
    setup_event_group = xEventGroupCreate();
    if (!setup_event_group) {
        ESP_LOGE(TAG, "Could not create setup event group. Insufficient heap!");
        fatal_error();
    }

    // Configure LED
    ESP_LOGV(TAG, "Configuring GPIO peripherals...");
    if (peripherals_gpio_setup() != 0) {
        ESP_LOGE(TAG, "Failed to configure GPIO peripherals!");
    }

    // Make sure the packet handling task is set up before the wireless module
    // is configured: This makes sure that all received packets can be handled
    // nicely.

    // Transmission handler for sending wireless packets to ICSB
    struct PacketTransmissionHandler wifi_icsb_left_handler = {
        .destination = InCanopyBoardLeft,
        .transmitter = &transmit_packet_wirelessly,
    };

    struct PacketTransmissionHandler wifi_icsb_right_handler = {
        .destination = InCanopyBoardRight,
        .transmitter = &transmit_packet_wirelessly,
    };

    struct PacketTransmissionHandler wifi_remote_handler = {
        .destination = WirelessRemoteBoard,
        .transmitter = &transmit_packet_wirelessly,
    };

    struct PacketTransmissionHandler spi_tx_handler = {
        .destination = MainControllerBoard,
        .transmitter = &transmit_packet_spi};

    struct PacketTransmissionHandler tx_handlers[] = {
        wifi_icsb_left_handler,
        wifi_icsb_right_handler,
        wifi_remote_handler,
        spi_tx_handler,
    };

    // Config for the packet handler: This is the receiver board
    struct PacketTaskConfig packet_config = {.here = ReceiverBoard,
                                             .num_transmission_handlers = 5,
                                             .tx_handlers = tx_handlers,
                                             .reception_handler =
                                                 &packet_received};

    if (xTaskCreate(&packet_task_entry, // entry function
                    "TASK_PCKT_HDLR",   // descriptive name
                    3000,               // stack size
                    &packet_config,     // argument to pass
                    PRIORITY_PCKT_TASK, // priority
                    &packet_task) == pdPASS) {
        ESP_LOGV(TAG, "Packet handler task started.");
    } else {
        fatal_error();
    }

    // Configure the ESP-NOW layer
    const char *mac_addresses[] = {
        CONFIG_ESPNOW_LEFT_ICSB_MAC,
        CONFIG_ESPNOW_RIGHT_ICSB_MAC,
        CONFIG_ESPNOW_WIRELESS_REMOTE_MAC,
    };

    const enum Location dests[] = {
        InCanopyBoardLeft,
        InCanopyBoardRight,
        WirelessRemoteBoard,
    };

    struct WirelessTaskConfiguration config = {
        .n_peers = 3,
        .mac_peers = mac_addresses,
        .dests = dests,
    };

    if (!wireless_transmission_setup(&config)) {
        ESP_LOGE(TAG, "Setup of ESP-NOW layer has failed.");
        fatal_error();
    }

    // Set up SPI interface
    if (!spi_interface_initialize()) {
        ESP_LOGE(TAG, "Setup of SPI interface failed!");
        fatal_error();
    }

    ESP_LOGV(TAG, "Starting SPI cleanup task.");
    if (xTaskCreate(&spi_interface_task, // entry function
                    "TASK_SPI_CLNP",     // descriptive name
                    3000,                // stack size
                    NULL,                // argument to pass
                    PRIORITY_SPIC_TASK,  // priority
                    &spi_task) == pdPASS) {
        ESP_LOGI(TAG, "SPI cleanup task started.");
    } else {
        fatal_error();
    }

    /* Setup the wake up task ----------------------------------------------- */

    ESP_LOGV(TAG, "Starting wakeup task.");
    if (xTaskCreate(&icsb_wakeup_entry, // entry function
                    "TASK_WKUP",        // descriptive name
                    4000,               // stack size
                    NULL,               // argument to pass
                    PRIORITY_WKUP_TASK, // priority
                    &wakeup_task) == pdPASS) {
        ESP_LOGI(TAG, "ICSB wakeup task started.");
    } else {
        fatal_error();
    }

    /* End of setup process ------------------------------------------------- */
    standard_loop(); // Shouldn't return from here. If we do, restart.

    esp_restart();
}

/* Operation modes ---------------------------------------------------------- */

void debug_loop(void) {
    Packet packet = {0};
    packet.origin = ReceiverBoard;
    packet.destination = InCanopyBoardLeft;
    packet.type = CommandPacket;
    packet.priority = PriorityNormal;
    packet.payload_size = 1;
    packet.payload = NULL;

    while (1) {
        packet.payload = malloc(sizeof(uint8_t) * 1);
        printf("Enter command number to send to ICSB: ");
        scanf("%hhi", (uint8_t *)packet.payload);
        if (*(uint8_t *)packet.payload == 32) {
            wake_icsb_start();
        }
        packet_add_to_queue(&packet);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    standard_loop();
}

void standard_loop(void) {
    while (1) {
        vTaskDelay(portMAX_DELAY);
    }
}

/* Error handling ----------------------------------------------------------- */

void fatal_error(void) {
    ESP_LOGE(TAG, "========================");
    ESP_LOGE(TAG, "===== FATAL ERROR ======");
    ESP_LOGE(TAG, "========================");

    vTaskSuspend(packet_task);
    gpio_set_level(LED_PIN, 1);

    while (true) {
        vTaskDelay(portMAX_DELAY);
    }
}

/* Packet handling ---------------------------------------------------------- */

void packet_received(Packet *packet) {
    // Release allocated payload pointer
    ESP_LOGD(TAG,
             "Packet contents: Origin %i, Type: %i, Priority: %i, "
             "Payload Size: %u bytes.",
             packet->origin, packet->type, packet->priority,
             packet->payload_size);

    if (packet->type == StatePacket && packet->payload != NULL) {
        uint8_t status = *(uint8_t *)packet->payload;
        // The status "3" maps to "Active" in the ICSB FSM
        if (status == 3) {
            // TODO: make this better. As soon as it changes in the ICSB, this
            // will definitely break
            wake_icsb_stop();
        }
    }

    free(packet->payload);
}
