/**
 *******************************************************************************
 * @file           spi-transmission.h
 * @brief          Declares the SPI slave that communicates with the STM32
 *                    mainboard.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __SPI_TRANSMISSION_H__
#define __SPI_TRANSMISSION_H__

#include "packets.h"
#include <stdbool.h>

/* Public functions --------------------------------------------------------- */

/**
 * Initialize the SPI interface with the parameters given in the sdkconfig.
 * @returns Whether the setup was successful.
 */
bool spi_interface_initialize(void);

/**
 * Task that handles the cleanup of the SPI transactions.
 *
 * This task handles the callback of successful SPI transmissions. It clears the
 * memory, and, if it received a packet in return from the main board, queues it
 * for further transmission.
 * @param arg Pass NULL.
 * @returns Doesn't return.
 */
void spi_interface_task(void *arg);

/**
 * Transmit a packet via SPI.
 * @param packet The packet to transmit.
 * @returns Whether handing the packet to the SPI driver was successful.
 */
int transmit_packet_spi(Packet *packet);

#endif /* __SPI_TRANSMISSION_H__ */
