/**
 *******************************************************************************
 * @file           spi-transmission.c
 * @brief          Implements the SPI slave that communicates with the STM32
 *                    mainboard.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "spi-transmission.h"
#include "driver/gpio.h"
#include "driver/spi_slave.h"
#include "esp_heap_caps.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include <string.h>

#define SPI_DEVICE SPI3_HOST
#define MOSI_PIN   CONFIG_SPI_MOSI_PIN
#define MISO_PIN   CONFIG_SPI_MISO_PIN
#define CS_PIN     CONFIG_SPI_CS_PIN
#define SCLK_PIN   CONFIG_SPI_SCK_PIN
#define READY_PIN  CONFIG_SPI_MAINBOARD_INTR_PIN
#define DMA_CHAN   2

/* Private variables -------------------------------------------------------- */
static const char *TAG = "spi-transmission";
static TaskHandle_t spi_cleanup_task_handle;

EventGroupHandle_t spi_events = NULL;

/* Private macros ----------------------------------------------------------- */

/** Designates completed SPI transaction setup. Can return to packets now. */
#define SPI_TRANS_SETUP (1 << 0)
/** Designates that an SPI transaction went through. Can cleanup memory now. */
#define SPI_TRANS_CPLTD (1 << 1)

/* Private function declarations -------------------------------------------- */

/**
 * Callback function when data is ready to be transmitted. Interrupt mainboard.
 *
 * This function is called by the SPI DMA controller. It should be placed in
 * IRAM according to the documentation about the SPI slave interface, this
 * ensures best performance.
 * This will drive the GPIO interrupt high to signal to the mainboard that data
 * is ready to be sent.
 * @param trans The transaction that is about to happen.
 */
static void IRAM_ATTR spi_ready_to_transmit(spi_slave_transaction_t *trans);

/**
 * Callback function when data has been transmitted. Release memory.
 *
 * This function is called by the SPI DMA controller. It should be placed in
 * IRAM according to the documentation about the SPI slave interface, this
 * ensures best performance.
 * This will drive the GPIO interrupt low and release the memory allocated in
 * IRAM.
 */
static void IRAM_ATTR spi_data_transmitted(spi_slave_transaction_t *trans);

/* Public functions --------------------------------------------------------- */

bool spi_interface_initialize(void) {
    // Defines which (software) SPI device should be used
    spi_host_device_t host_device = SPI_DEVICE; // use SPI device from config

    // Set up the BUS
    spi_bus_config_t bus_config = {.mosi_io_num = (gpio_num_t)MOSI_PIN,
                                   .miso_io_num = (gpio_num_t)MISO_PIN,
                                   .sclk_io_num = (gpio_num_t)SCLK_PIN,
                                   .quadhd_io_num = -1,
                                   .quadwp_io_num = -1};

    // Configure the slave properties
    spi_slave_interface_config_t slave_config = {
        .spics_io_num = (gpio_num_t)CS_PIN,
        .flags = 0,
        .queue_size = 30,
        .mode = 3, ///< Needs to be 1 or 3 for DMA
        .post_setup_cb = spi_ready_to_transmit,
        .post_trans_cb = spi_data_transmitted};

    esp_err_t ret = ESP_OK;
    if ((ret = spi_slave_initialize(host_device, &bus_config, &slave_config,
                                    DMA_CHAN)) != ESP_OK) {
        ESP_LOGE(TAG, "Could not initialize SPI slave. Reason: %s",
                 esp_err_to_name(ret));
        return false;
    }

    // set up the GPIO pin used to signal data readiness to the main board
    gpio_config_t rdy_conf = {.pin_bit_mask = 1 << READY_PIN,
                              .mode = GPIO_MODE_OUTPUT,
                              .pull_up_en = GPIO_PULLUP_DISABLE,
                              .pull_down_en = GPIO_PULLDOWN_DISABLE,
                              .intr_type = GPIO_INTR_DISABLE};

    ret = gpio_config(&rdy_conf);
    if (ret != ESP_OK) {
        ESP_LOGE(
            TAG,
            "Could not initialize GPIO for interrupt capability. Reason: %s",
            esp_err_to_name(ret));
        return false;
    }

    spi_events = xEventGroupCreate();
    assert(spi_events != NULL);

    ESP_LOGI(TAG, "SPI interface successfully set up.");

    return true;
}

void spi_interface_task(void *arg) {
    spi_cleanup_task_handle = xTaskGetCurrentTaskHandle();

    while (1) {
        // block for a notification that cleanup can happen
        xEventGroupWaitBits(spi_events, SPI_TRANS_CPLTD, pdTRUE, pdTRUE,
                            portMAX_DELAY);
        ESP_LOGV(TAG, "Cleanup task unlocked!");

        // inspect transaction struct
        spi_slave_transaction_t *trans = NULL;

        // because the task was unblocked, the transaction result should be
        // available immediately
        esp_err_t ret = spi_slave_get_trans_result(SPI_DEVICE, &trans, 0);
        if (ret == ESP_OK) {
            if (trans != NULL) {
                ESP_LOGV(TAG, "Processing reply from SPI.");
                heap_caps_free((void *)trans->tx_buffer);

                uint32_t *rx_buf = (uint32_t *)trans->rx_buffer;
                ESP_LOGI(TAG, "Transaction length: %i", trans->trans_len);
                if (rx_buf[0] != 0 && trans->trans_len >= 5) {
                    ESP_LOGI(TAG, "Byte 0 or RX-BUF is %i", rx_buf[0]);
                    ESP_LOGV(TAG, "SPI receive buffer is non-empty.");

                    // rx buffer is 256 bytes, so we're safe reading up to 256
                    // bytes anyway

                    uint32_t first_word = rx_buf[0];
                    Packet packet = {
                        .origin = (first_word & 0xff000000) >> 24,
                        .destination = (first_word & 0x00ff0000) >> 16,
                        .type = (first_word & 0x0000ff00) >> 8,
                        .priority = (first_word & 0x000000ff),
                        .payload_size = (rx_buf[1] & 0xff000000) >> 24,
                        .payload = NULL};

                    if ((rx_buf[1] & 0x00ffffff) != 0x00000000) {
                        ESP_LOGE(
                            TAG,
                            "Received packet does not have three empty bytes.");
                        // just skip the packet
                    } else {
                        // this packet seems legit
                        if (packet.payload_size > 0) {
                            packet.payload =
                                malloc(sizeof(packet.payload_size));
                            memcpy(packet.payload, rx_buf + 2,
                                   packet.payload_size);
                        }

                        if (packet_add_to_queue(&packet) != 0) {
                            ESP_LOGE(TAG, "Failed to put packet in queue.");
                            free(packet.payload);
                        }
                    }
                }

                heap_caps_free((void *)trans->rx_buffer);
                free((void *)trans);
            }
        } else {
            ESP_LOGE(TAG, "Could not get transaction result. Reason: %s",
                     esp_err_to_name(ret));
        }
    }
}

int transmit_packet_spi(Packet *packet) {
    uint32_t *tx_buf __attribute__((aligned(4))) =
        NULL; // buffer in IRAM to pass to the SPI DMA
    uint32_t *rx_buf __attribute__((aligned(4))) = NULL; // same

    // this should also be a pointer, because multiple calls to transmit a
    // packet could occur before one is sent out
    spi_slave_transaction_t *trans = NULL;

    if (!packet || packet->payload_size >= 250) {
        ESP_LOGI(TAG,
                 "SPI transmission handler called with an invalid packet.");
        goto fail;
        return -1;
    }

    trans = malloc(sizeof(spi_slave_transaction_t));
    if (!trans) {
        ESP_LOGE(TAG, "Failed to allocate memory for SPI transaction struct.");
        goto fail;
    }

    // allocate memory in the DMA-accessible region for the RX/TX buffers
    // note that with every SPI packet, 0.5 KiB are allocated as buffers – make
    // sure to free them ASAP! IRAM has only 192 KiB size, and other things are
    // running as well :-)
    tx_buf = heap_caps_malloc(256 * sizeof(uint8_t), MALLOC_CAP_DMA);
    rx_buf = heap_caps_malloc(256 * sizeof(uint8_t), MALLOC_CAP_DMA);

    if (!tx_buf || !rx_buf) {
        ESP_LOGE(TAG, "Failed to allocate DMA memory for SPI transmission!"
                      "Have to drop packet...");
        goto fail;
    }

    memset(tx_buf, 0, 256);
    memset(rx_buf, 0, 256);

    if (!tx_buf || !rx_buf) {
        ESP_LOGE(TAG,
                 "Failed to allocate memory in IRAM for SPI transmission.");
        goto fail;
    }

    // limitation of the IRAM: only four-byte aligned access possible
    uint32_t first_word = (((uint32_t)(packet->origin)) << 24) +
                          (((uint32_t)(packet->destination)) << 16) +
                          (((uint32_t)(packet->type)) << 8) +
                          ((uint32_t)(packet->priority));
    uint32_t second_word = ((uint32_t)packet->payload_size) << 24;

    // 5 bytes are filled with header content, then 3 bytes of zeros, then
    // payload
    tx_buf[0] = first_word;
    tx_buf[1] = second_word;
    memcpy(tx_buf + 2, packet->payload, packet->payload_size);

    trans->length = 256 * 8;   // transmitting 256 bytes, always
    trans->trans_len = 0;      // this will be populated by received data
    trans->tx_buffer = tx_buf; // pointer to our tx buffer
    trans->rx_buffer = rx_buf; // pointer to our rx buffer
    trans->user = NULL;        // we don't need the user field

    // Try to queue packet. If queue is full, wait for 10ms before dropping it.
    esp_err_t ret =
        spi_slave_queue_trans(SPI_DEVICE, trans, 0 / portTICK_PERIOD_MS);

    if (ret != ESP_OK) {
        // failed to put in queue, release memory
        ESP_LOGE(TAG, "Failed to queue packet for SPI transmission, reason: %s",
                 esp_err_to_name(ret));
        goto fail;
    }

    // success
    ESP_LOGV(TAG, "Successfully scheduled packet for SPI transmission.");
    free(packet->payload);

    // wait for transaction setup to be complete, otherwise a race condition
    // in the f***ing driver can occur (sic!)
    // wait forever, for all bits of SPI_TRANS_SETUP, and clear them on exit
    xEventGroupWaitBits(spi_events, SPI_TRANS_SETUP, pdTRUE, pdTRUE,
                        50);

    return 0;

fail:
    heap_caps_free(tx_buf);
    heap_caps_free(rx_buf);
    free(trans);
    free(packet->payload); // do this avoid memory leaks
    return -1;
}

/* Private function implementations ----------------------------------------- */

static void IRAM_ATTR spi_ready_to_transmit(spi_slave_transaction_t *trans) {
    gpio_set_level(READY_PIN, 1);
    gpio_set_level(13, 1);

    // would love to unlock here, but that's too early – doesn't work
    /*BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      xEventGroupSetBitsFromISR(spi_events, SPI_TRANS_SETUP,
    //                          &xHigherPriorityTaskWoken);
    if (xHigherPriorityTaskWoken == pdTRUE)
        portYIELD_FROM_ISR();*/
}

/**
 * Callback function when data has been transmitted. Release memory.
 *
 * This function is called by the SPI DMA controller. It should be placed in
 * IRAM according to the documentation about the SPI slave interface, this
 * ensures best performance.
 * This will drive the GPIO interrupt low and release the memory allocated in
 * IRAM.
 */
static void IRAM_ATTR spi_data_transmitted(spi_slave_transaction_t *trans) {
    // lower GPIO interrupt
    gpio_set_level(READY_PIN, 0);
    gpio_set_level(13, 0);

    // unblock the cleanup task and yield if it is now the highest priority
    // task
    BaseType_t xHigherPriorityTaskWoken1 = pdFALSE;
    BaseType_t xHigherPriorityTaskWoken2 = pdFALSE;
    xEventGroupSetBitsFromISR(spi_events, SPI_TRANS_SETUP,
                              &xHigherPriorityTaskWoken1);

    xEventGroupSetBitsFromISR(spi_events, SPI_TRANS_CPLTD,
                              &xHigherPriorityTaskWoken2);
    if (xHigherPriorityTaskWoken1 == pdTRUE ||
        xHigherPriorityTaskWoken2 == pdTRUE)
        portYIELD_FROM_ISR();
}
