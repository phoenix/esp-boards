/**
 *******************************************************************************
 * @file           peripherals.c
 * @brief          Implements the setup of all peripherals that the ESP32 uses
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>
#include <string.h>

#include "esp_log.h"
#include "esp_netif.h"
#include "esp_now.h"
#include "esp_wifi.h"

#include "debug.h"
#include "peripherals.h"

/* Local defines -------------------------------------------------------------*/

// GPIO
// These values come from the HUZZAH32 feather data sheet
gpio_num_t LED_PIN = 13;

static const char *TAG = "peripherals";

/* Setup functions ---------------------------------------------------------- */

int peripherals_gpio_setup() {

    // Configure LED
    ESP_LOGV(TAG, "Configuring LED...");
    gpio_config_t led_conf = {.pin_bit_mask = 1 << LED_PIN,
                              .pull_up_en = GPIO_PULLUP_DISABLE,
                              .pull_down_en = GPIO_PULLDOWN_DISABLE,
                              .mode = GPIO_MODE_OUTPUT,
                              .intr_type = GPIO_INTR_DISABLE};
    gpio_config(&led_conf);
    ESP_LOGV(TAG, "LED done.");

    return 0;
}
