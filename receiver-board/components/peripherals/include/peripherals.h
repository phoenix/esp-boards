/**
 *******************************************************************************
 * @file           peripherals.h
 * @brief          Header file for all the peripherals used by the ESP-32.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __PERIPHERALS_H__
#define __PERIPHERALS_H__

#include "driver/gpio.h"
#include "driver/i2c.h"

/** The GPIO pin where the red user LED is connected. */
extern gpio_num_t LED_PIN;

/* Setup functions ---------------------------------------------------------- */

/**
 * Set up the GPIO ports.
 * @param none
 * @returns Non-zero on error.
 */
int peripherals_gpio_setup(void);

/**
 * Set up the WiFi peripheral.
 *
 * Initialize the WiFi module.
 * @param none
 * @returns Non-zero on error.
 */
int peripherals_wifi_setup(void);

/*
 * Convert a MAC address in the format "ff:ff:ff:ff:ff:ff" to its representation
 * with integers.
 * @param string The string to convert
 * @param dest A pointer to the destination array.
 * @returns Whether the action was successful. Non-zero on error.
 * @pre The destination array must have length >= ESP_NOW_ETH_ALEN
 */
int mac_addr_from_string(const char *string, uint8_t dest[])
    __attribute((nonnull));

#endif /* __PERIPHERALS_H__ */
