/**
 *******************************************************************************
 * @file           icsb-wakeup.h
 * @brief          Interface for waking the ICSB up in a "polling" fashion
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

/* Public functions --------------------------------------------------------- */

/**
 * Entry function of the ICSB Wakeup task.
 *
 * The ICSB wakeup task will wait to be started via @see wake_icsb_start and
 * will add an "Activate" command to the packets queue every second. The ICSB
 * will enable the WiFi module for two seconds, so the packet should be
 * received in this slot.
 */
void icsb_wakeup_entry(void *arg);

/**
 * Start the ICSB wakeup task.
 * @returns true if the task could be started, false otherwise
 */
bool wake_icsb_start(void);

/**
 * stop the ICSB wakeup task.
 * @returns true if the task could be stopped, false otherwise
 */
bool wake_icsb_stop(void);
