/**
 *******************************************************************************
 * @file           icsb-wakeup.c
 * @brief          Provides an ICSB wake-up method by "polling" the WiFi
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "main.h"

#include "icsb-wakeup.h"
#include "packets.h"

/* Local macros ------------------------------------------------------------- */

/** Flag used to indicate that the ICSB wake up should start. */
#define FLAG_START_ICSB_WAKEUP (1U << 17)
/** Flag used to indicate that the ICSB wake up should stop. */
#define FLAG_STOP_ICSB_WAKEUP (1U << 18)

/* Private variables -------------------------------------------------------- */

/**
 * @brief A switch that can switch the ICSB task on or off.
 *
 * This bool is checked on each execution of the loop to check whether another
 * "activate" packet should be sent to the ICSB or not.
 */
static volatile bool icsb_task_active = false;

static const char *TAG = "icsb-wakeup";

/* Private functions -------------------------------------------------------- */

/**
 * Interrupt service routine for the external interrupt received to wake ICSBs.
 *
 * Placed in IRAM for best performance.
 * @param arg This will be the GPIO number that triggered the interrupt.
 */
static void IRAM_ATTR gpio_isr_handler(void *arg);

/* Public function implementation ------------------------------------------- */

void icsb_wakeup_entry(void *arg) {
    // The way this task works: It will run every second and check whether the
    // "active" flag is set. If it is, send out an "Activate" packet to ICSBs.
    TickType_t last_wake = xTaskGetTickCount();

    // Set up the GPIO port set in the sdkconfig as pulled down external
    // interrupt with rising edge detection.
    gpio_config_t exti_config = {
        .mode = GPIO_MODE_INPUT,              // input pin
        .pull_down_en = GPIO_PULLDOWN_ENABLE, // pulled down so not floating
        .pin_bit_mask = (1U << CONFIG_ICSB_WAKEUP_EXTI_PIN), // bit mask
        .intr_type = GPIO_INTR_POSEDGE, // rising edge trigger
    };

    esp_err_t ret = gpio_config(&exti_config);
    if (ret != ESP_OK) {
        fatal_error();
    }

    // Configure the ISR that handles the GPIO interrupt
    gpio_install_isr_service(0);
    gpio_isr_handler_add(CONFIG_ICSB_WAKEUP_EXTI_PIN, gpio_isr_handler,
                         (void *)CONFIG_ICSB_WAKEUP_EXTI_PIN);

    while (true) {

        if (icsb_task_active) {
            ESP_LOGI(TAG, "Start waking ICSB!");
            // The ICSB packet goes from the receiver board to both ICSBs, so
            // set the common attributes here. Packet struct  is allocated on
            // the stack, the payload on the heap.
            Packet wakeup_packet = {
                .origin = ReceiverBoard,
                .type = CommandPacket,
                .priority = 1,
                .payload_size = 1,
            };

            // Remember the memory invariant: Allocation for payload happens
            // BEFORE adding the packet to the queue and the transmission
            // handler frees it

            // Left ICSB
            wakeup_packet.destination = InCanopyBoardLeft;
            wakeup_packet.payload = malloc(1);
            *((uint8_t *)wakeup_packet.payload) = CommandActivate;
            packet_add_to_queue(&wakeup_packet);

            // Right ICSB
            wakeup_packet.destination = InCanopyBoardRight;
            wakeup_packet.payload = malloc(1);
            *((uint8_t *)wakeup_packet.payload) = CommandActivate;
            packet_add_to_queue(&wakeup_packet);
        }

        // Delay until the full second has passed.
        vTaskDelayUntil(&last_wake, 1000 / portTICK_PERIOD_MS);
    }
}

bool wake_icsb_start(void) {
    // Starting can be done by just flipping the volatile bool on.
    icsb_task_active = true;
    return true;
}

bool wake_icsb_stop(void) {
    // Stopping can be done by just setting the volatile bool to false.
    icsb_task_active = false;
    return true;
}

/* Private function implementation ------------------------------------------ */

static void IRAM_ATTR gpio_isr_handler(void *arg) {
    uint32_t gpio_num = (uint32_t)arg;
    wake_icsb_start();
    if (gpio_num == CONFIG_ICSB_WAKEUP_EXTI_PIN) {
        wake_icsb_start();
    }
}
