# Receiver Board

The role of the receiver board in our project is critical: It acts as the link between the two in-canopy sensor boards (ICSBs) and the main microcontroller.
Its tasks are:

* Sending commands issued from the main microcontroller to the ICSBs via WiFi.
* Forwarding sensor data and log data from the ICSBs to the main microcontroller
via a serial connection.
* Act as a debugging point for the in-canopy sensor boards during development, but also when they are already built into the parachute and can't be accessed physically.

## Hardware
The receiver board is an ESP32-based dual-core microcontroller. We are currently using the [HUZZAH32](https://www.adafruit.com/product/3405) "Feather" board from AdaFruit.

## Software Architecture

The receiver board runs FreeRTOS. On startup, it will perform the following
tasks:

1. Print debug/release configuration to the serial terminal (UART0).

2. Start the packet handling task, which gets the highest priority on this board.

3. Configure all necessary peripherals (LED, SPI, WiFi, ...).

4. Wait for 10 seconds for input from the serial terminal. If "debug" was read, enter debug mode.

5. If not input was received, discard debug mode and sleep main task.

   

### Receiver Board Modules

These modules run only on the receiver board. Find the shared components in the top-level `README.md` of the repository. Many of the modules contain functions that will run as [Tasks](#Tasks).

| Module Name        | What does it do?                                             | Ready for Review? | Reviewed / unchanged? |
| ------------------ | ------------------------------------------------------------ | ----------------- | --------------------- |
| `peripherals`      | Set up the ESP-NOW protocol.                                 | ✅                 |                       |
| `spi-transmission` | SPI slave task that accepts packets, adds them to the SPI queue and sends an interrupt to the main board to run a connection | ✅                 |                       |
| `main`             | Sets up all the other tasks.                                 | ✅                 |                       |

## Reference Manual

### Pinout

The nominal pinout of the board (if the `sdkconfig` is not changed after cloning) is as follows:

* `GND`: Ground, connect to ground of STM32 for common ground

* GPIO 4: `CS` pin, connect to the `CS` pin of the STM32.

* GPIO 5: `SCK` pin, connect to STM32 `SCK`.

* GPIO 16: `READY` interrupt for STM32, connect to corresponding `GPIO_EXTI` of STM32.

* GPIO 17: `ICSB_WAKEUP` interrupt for ESP32, starts the "ICSB wakeup" task

* GPIO 18: `MOSI`, connect to STM32 `MOSI`.

* GPIO 19: `MISO`, connect to STM32 `MISO`.

  