/**
 *******************************************************************************
 * @file           sleep.h
 * @brief          Interface for sleeping the ICSB until receiving a wake-up
 *                      packet.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

/**
 * Enter the ICSB sleep mode.
 *
 * In the sleep mode, the WiFi module is powered down and disabled. The ICSB
 * sleeps for 1 minute and then wakes up for 2 seconds, waiting for a "wake-up"
 * command packet to be received. If not, it will continue sleeping for 1
 * minute.
 * This function will return once the sleeping should end.
 */
void icsb_sleep_until_woken(void);