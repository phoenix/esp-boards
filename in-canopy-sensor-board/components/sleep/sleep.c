/**
 *******************************************************************************
 * @file           sleep.c
 * @brief          Interface for sleeping the ICSB until receiving a wake-up
 *                      packet.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "esp_log.h"
#include "esp_now.h"
#include "esp_sleep.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "main.h"
#include "sleep.h"

/** Duration that the ICSB sleeps before waking up. Two seconds are subtracted
 * for the time that the WiFi module is powered on. */
#define SLEEP_DURATION_USEC (CONFIG_BNO_SLEEP_MIN * 60000000U - 2000000U)

/* Debugging and logging ---------------------------------------------------- */

static const char *TAG = "sleep";

/* Public function implementation ------------------------------------------- */

void icsb_sleep_until_woken() {
    // Sensors were already stopped from sampling

    esp_err_t ret;

    do {
        // Power down WiFi
        ret = esp_wifi_stop();
        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "Could not power down WiFi!");
            return;
        }

        // Enable timer as wakeup source: 60'000'000 µs = 1 minute
        ret = esp_sleep_enable_timer_wakeup(SLEEP_DURATION_USEC);
        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "Could not enable timer as wakeup source.");
            return;
        }

        ret = esp_light_sleep_start();

        if (ret != ESP_OK) {
            ESP_LOGE(TAG, "Did not enter light sleep. WiFi not disabled?");
            return;
        }

        ESP_LOGI(TAG,
                 "Waking up from light sleep to check whether to activate...");

        // Re-enable WiFi and wait for 2 seconds, whether the wake-up flag is
        // raised
        esp_wifi_start();

        // This task runs on the main thread, where FSM transitions are invoked.
        // This means we can check whether a transition is received here, and
        // return if necessary.
    } while ((ulTaskNotifyTake(pdFALSE, 2000 / portTICK_PERIOD_MS) &
              Activate) == 0x0);

    // Set activation bit again before returning, because it was cleared in the
    // while() condition
    xTaskNotify(xTaskGetCurrentTaskHandle(), Activate, eSetBits);

    return;
}