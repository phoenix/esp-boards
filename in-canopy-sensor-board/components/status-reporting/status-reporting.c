/**
 *******************************************************************************
 * @file           status-reporting.c
 * @brief          A task that repeatedly reports the status of the ICSB to
 *                      the mainboard.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdbool.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "battery.h"
#include "esp_log.h"
#include "main.h"
#include "packets.h"
#include "status-reporting.h"

/* Private variables -------------------------------------------------------- */

static const char *TAG = "status-report";

/* Public functions --------------------------------------------------------- */

void status_report_entry(void *arg) {

    // Setup of the battery reading is necessary (mainly configuring ADC
    // channel and checking which reference voltage can be used)
    battery_setup();

    while (true) {
        enum SystemState state = get_system_state();

        uint16_t battery_voltage = battery_read_voltage();
        ESP_LOGI(TAG, "Battery voltage: %u mV", battery_voltage);

        Packet state_packet = {.origin = icsb_location,
                               .destination = MainControllerBoard,
                               .type = StatePacket,
                               .priority = 0,
                               .payload_size = 2};

        state_packet.payload = malloc(2 * sizeof(uint8_t));

        if (battery_voltage >= 5120) {
            // we exceed uint8_t value range after dividing by 20, set it to 0
            ESP_LOGE(TAG, "Battery voltage range exceeded!");
            battery_voltage = 0;
        }

        // First byte is the FSM state, second byte is battery in mV / 100
        *((uint8_t *)state_packet.payload) = (uint8_t)state;
        *(((uint8_t *)state_packet.payload) + 1) = battery_read_voltage() / 20;
        // casting to uint8_t is fine since the battery voltage shouldn't exceed
        // 4200 mV

        packet_add_to_queue(&state_packet);

        vTaskDelay(1000 / CONFIG_STATUS_REPORT_FREQ / portTICK_PERIOD_MS);
    }
}