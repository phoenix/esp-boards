/**
 *******************************************************************************
 * @file           status-reporting.h
 * @brief          A task that repeatedly reports the status of the ICSB to
 *                      the mainboard.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __STATUS_REPORTING_H__
#define __STATUS_REPORTING_H__

/**
 * Entry function of the status reporting task.
 *
 * The status reporting task will send the current status of the ICSB to the
 * mainboard at a predefined rate.
 * @note This function is called upon task creation in the main task.
 *  Do not call manually. Won't return.
 */
void status_report_entry(void *arg) __attribute__((noreturn));

#endif /* __STATUS_REPORTING_H__ */