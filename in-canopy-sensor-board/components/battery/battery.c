/**
 *******************************************************************************
 * @file           battery.c
 * @brief          Operates the GPIO pin #35 to read VBAT
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <stdint.h>

#include "driver/adc.h"

#include "battery.h"
#include "esp_adc_cal.h"
#include "esp_log.h"

/* Local macros ------------------------------------------------------------- */

/** The default reference voltage that the ADC input is compared against.
        Normally not used since there is a more accurate value in the eFuse. */
#define DEFAULT_VREF 1100

/* Local variables ---------------------------------------------------------- */

/// Logging tag for the ESP_LOG* functions
static const char *TAG = "battery";
static esp_adc_cal_characteristics_t *adc_chars = NULL;

/* Public function implementation ------------------------------------------- */

void battery_setup(void) {

    // Setup the channel with the following specs:
    //      12 bit resolution, channel 7 (gpio #35),
    //      11 dB attenuation (recommended in the documentation, don't ask me)
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_DB_11);

    // Determine the reference voltage (maybe it's in the eFuse, maybe not)
    // to use during conversion from raw to the actual voltage
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(
        ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);

    switch (val_type) {
        case ESP_ADC_CAL_VAL_EFUSE_VREF:
            ESP_LOGI(TAG, "VREF is in fuse.");
            break;
        case ESP_ADC_CAL_VAL_EFUSE_TP:
            ESP_LOGI(TAG, "Two point VREFs in fuse.");
            break;
        default:
            ESP_LOGI(TAG, "No VREF in fuse, using default.");
            break;
    }
}

uint16_t battery_read_voltage(void) {
    int adc_reading = adc1_get_raw(ADC1_CHANNEL_7);
    uint16_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
    return 2 * voltage;
}
