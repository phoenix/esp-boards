/**
 *******************************************************************************
 * @file           battery.h
 * @brief          Operates the GPIO pin #35 to read VBAT
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdint.h>

/**
 * @brief Setup the battery reading pin.
 *
 * Configures the GPIO pin #35 for taking ADC measurements.
 */
void battery_setup(void);

/**
 * @brief Reads the current battery voltage.
 * @returns Current battery voltage in millivolts
 */
uint16_t battery_read_voltage(void);
