/**
 ******************************************************************************
 * @file           status-indicator.c
 * @brief          Implements a status indicator LED.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include "status-indicator.h"

#include "driver/gpio.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/* Private variables -------------------------------------------------------- */

/** GPIO pin connected to the red LED next to the USB port (cf. datasheet) */
static gpio_num_t STATUS_LED_PIN = 13;

/** Tag for the log messages printed by the ESP_LOG(*) macros. */
static const char *TAG = "status-indicator";

/** Number of consequential blinks in one burst. */
static uint_fast8_t num_blinks = 0;

void status_indicator_task(void *arg) {
    // First configure status LED
    ESP_LOGV(TAG, "Configuring status LED...");
    gpio_config_t led_conf = {.pin_bit_mask = 1 << STATUS_LED_PIN,
                              .pull_up_en = GPIO_PULLUP_DISABLE,
                              .pull_down_en = GPIO_PULLDOWN_DISABLE,
                              .mode = GPIO_MODE_OUTPUT,
                              .intr_type = GPIO_INTR_DISABLE};
    esp_err_t ret = gpio_config(&led_conf);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to configure LED. Reason: %s",
                 esp_err_to_name(ret));
        vTaskDelay(portMAX_DELAY);
    }

    ESP_LOGV(TAG, "Status LED configured.");

    TickType_t tick = xTaskGetTickCount();
    while (1) {
        // Yes, this int is not protected by a semaphore. But because this task
        // really isn't critical, I'll let it slip. Not a huge problem if it
        // gets changed during the loop.
        if (num_blinks == 0) {
            // special option: num_blinks == 0 means always on
            WRITE_PERI_REG(GPIO_OUT_W1TS_REG,
                           (1 << STATUS_LED_PIN)); // LED high
        }

        for (uint_fast8_t i = 0; i < num_blinks; i++) {
            WRITE_PERI_REG(GPIO_OUT_W1TS_REG,
                           (1 << STATUS_LED_PIN)); // LED high
            vTaskDelay(100 / portTICK_PERIOD_MS);
            WRITE_PERI_REG(GPIO_OUT_W1TC_REG, (1 << STATUS_LED_PIN)); // LED low
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }

        vTaskDelayUntil(&tick, 1000 / portTICK_PERIOD_MS);
    }
}

void status_indicator_set_burst_number(uint_fast8_t num) {
    if (num <= 10)
        num_blinks = num;
}