/**
 ******************************************************************************
 * @file           status-indicator.h
 * @brief          Provides a status indicator LED interface.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#ifndef __STATUS_INDICATOR_H__
#define __STATUS_INDICATOR_H__

#include <stdint.h>

/**
 * Entry function of the status indicator task.
 *
 * The status indicator task does nothing but blink the RED user LED in a
 * specified pattern. It should run at a low priority.
 */
void status_indicator_task(void *arg);

/**
 * Set number of blinks in one burst blink rate.
 * @param num Number of consequential blinks in one burst, less than 10.
 */
void status_indicator_set_burst_number(uint_fast8_t num);

#endif /* __STATUS_INDICATOR_H__ */