/**
 *******************************************************************************
 * @file           sensors.h
 * @brief          Declares the sensor sampling task and useful structs for
 *                      passing the sensor data around.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <stdbool.h>

#include "bno-055.h"
#include "data.h"

/* FreeRTOS Task Functions -------------------------------------------------- */

/**
 * Entry function to the sensor sampling task.
 *
 * Creation, suspension and resumption of this task happen all through
 * transitions in the finite state machine
 * @warning Do not invoke this method after creation.
 * @param arg Thread arguments, pass NULL or whatever you'd like; it is ignored.
 * @returns Doesn't return.
 */
void sensors_task_entry(void *arg);

/**
 * Start actively sampling the sensors at the predefined rates.
 * @returns Whether the timer activating the sensor could be started.
 * @pre The sensor sampling task entry function must have been called.
 */
bool sensors_start_sampling(void);

/**
 * Stop actively sampling the sensors.
 * @returns Whether the timer activating the smapling could be stopped.
 * @pre The sensor sampling task entry function must have been called.
 */
bool sensors_stop_sampling(void);

/**
 * @brief Wakes the sensors from their low-power configurations.
 *
 * If the sensors lost their calibration in the low-power mode, it will also
 * re-calibrate them before returning.
 * @returns true if the wakeup was successful, false otherwise
 */
bool sensors_wakeup(void);

/**
 * @brief Puts the sensors into a low-power configuration.
 * @returns true if enabling the low-power configuration was successful.
 */
bool sensors_enable_lowpower(void);

/**
 * @brief Calibrate the sensors. Blocks until they are calibrated.
 *
 * This function does not actually "calibrate" the sensors, it rather polls the
 * calibration registers to see when the BNO055 has completed its internal
 * calibration routine. It will not return until calibration has succeeded.
 * @pre The sensors must have previously been woken up with @see sensors_wakeup.
 *      If this has not happened, calibration cannot succeed.
 */
void sensors_calibrate(void);

#endif /* __SENSORS_H__ */
