/**
 ******************************************************************************
 * @file           sensors.c
 * @brief          Implements the sensor sampling task.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"

#include "bmp-388.h"
#include "bno-055.h"
#include "debug.h"
#include "main.h"
#include "packets.h"
#include "sensors.h"
#include "serialization.h"
#include "status-indicator.h"

/* Local macros ------------------------------------------------------------- */

/** Number of consecutive sampling errors that may occur before the bus is
 * reset. */
#define NUM_SAMPLING_ERRORS_BEFORE_RESET 2

/* Private variables -------------------------------------------------------- */

static const char *TAG = "sensors";

static TimerHandle_t bno055_timer = NULL;
static TaskHandle_t sensor_task = NULL;

/** The handle used for the BNO055 9-axis IMU */
BNO055_Handle bno_055 = {
    .i2c_port_num = I2C_NUM_0,
    .reset_pin = CONFIG_BNO_RESET_PIN,
    .afsr = ACCEL_FSR4G,
    .gfsr = GYRO_FSR250,
};

BMP388_Handle bmp_388 = {
    .i2c_port_num = I2C_NUM_0,
    .bmp388_i2c_addr = 0x77,
};

/* Private functions -------------------------------------------------------- */

/**
 * Callback function for the software timer run by FreeRTOS.
 * Unblocks the sensor sampling task.
 * @param timer The software timer that fired.
 */
void timer_callback(TimerHandle_t timer);

/* Public functions --------------------------------------------------------- */

void sensors_task_entry(void *arg) {
    ESP_LOGD(TAG, "Entered sensor task.");

    // Set the handle to be used in the timer callback for notifying callback
    sensor_task = xTaskGetCurrentTaskHandle();

    // Create the software timer that will fire each time we should sample the
    // sensor, but don't start it yet
    if ((bno055_timer = xTimerCreate(
             "bno055timer", pdMS_TO_TICKS(1000 / CONFIG_BNO_055_SAMPLE_RATE),
             pdTRUE, NULL, &timer_callback)) == NULL) {
        ESP_LOGE(TAG, "Failed to set up software timer for BNO-055!");
    }

    uint8_t bno_sample_errors;
    uint8_t bmp_sample_errors;

sensors_setup:
    // This code block sets up the sensors, does the self-test and pushes the
    // calibration values. We can jump to this location in an attempt to recover
    // the bus in case a sensor fails.
    ESP_LOGV(TAG, "Configuring BNO-055...");

    if (!bno055_setup(&bno_055)) {
        ESP_LOGE(TAG, "Failed to set up BNO-055!");
    } else {
        ESP_LOGI(TAG, "BNO-055 set up.");
    }

    if (!bmp388_init(&bmp_388)) {
        ESP_LOGE(TAG, "Failed to set up BMP-388!");
    } else {
        ESP_LOGI(TAG, "BMP-388 set up.");
    }

    // Push the calibration values right on start to give the sensor a head
    // start in self-configuring itself.
    if (!bno055_push_calibration(&bno_055)) {
        ESP_LOGI(TAG, "Failed to push calibration values to BNO055...");
    }

    Packet packet = {0};
    packet.origin = icsb_location;
    packet.destination = MainControllerBoard;
    packet.type = DataPacket;
    packet.priority = PriorityNormal;
    packet.payload_size = 23;
    struct DataPayload data_payload;

    // Sampling while loop. Will only be broken out of in case a sensor failure
    // is detected and we try to set up the sensors again.
    bno_sample_errors = 0;
    bmp_sample_errors = 0;
    while (true) {

        // Blocks here until unlocked by the timer callback
        uint32_t not_val = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        if (not_val != 0) {
            // Payload dynamically allocated by packet creator and freed by
            // transmission handler
            packet.payload = malloc(25);
            data_payload.sensor_type = BNO055_Sensor;
            if (!bno055_sample(&bno_055, &data_payload.bno_readout)) {
                ESP_LOGE(TAG, "Failed to sample BNO-055!");
                free(packet.payload);
                bno_sample_errors++;
            } else {
                bno_sample_errors = 0;
                serialize_data_payload(&data_payload, packet.payload);
                // Hand packet over to the packets task, but remember to free if
                // the queue is already full
                if (packet_add_to_queue(&packet) != 0)
                    free(packet.payload);
            }

            // sample BMP
            packet.payload = malloc(25);
            data_payload.sensor_type = BMP388_Sensor;
            if (bmp388_sample(&bmp_388, &data_payload.bmp_readout) != true) {
                // that's an error
                ESP_LOGE(TAG, "Failed to sample BMP-388!");
                free(packet.payload);
                bmp_sample_errors++;
            } else {
                bmp_sample_errors = 0;
                serialize_data_payload(&data_payload, packet.payload);
                // Hand packet over to the packets task, but remember to free if
                // the queue is already full
                if (packet_add_to_queue(&packet) != 0)
                    free(packet.payload);
            }

            // If we had too many errors, we break out of the loop and reset.
            // Only the BNO can trigger a reset. If the BMP fails, we just let
            // it silently fail.
            if (bno_sample_errors >= NUM_SAMPLING_ERRORS_BEFORE_RESET) {
                goto sensors_setup;
            }
        }
    }
}

void sensors_calibrate() {
    // light up LED while calibrating
    status_indicator_set_burst_number(0);

    uint32_t current_tick = xTaskGetTickCount();
    uint8_t fully_calibrated_ticks =
        0; // subsequent ticks where CALIB_STAT was 3

    while (true) {
        // get current system calibration status
        if (bno055_is_calibrated(&bno_055)) {
            fully_calibrated_ticks++;
        } else {
            // What did it cost you? Everything. Back to the beginning.
            fully_calibrated_ticks = 0;
        }

        if (fully_calibrated_ticks > 100) {
            // can only return here
            break;
        }

        vTaskDelayUntil(&current_tick, 50 / portTICK_PERIOD_MS);
    }

    bno055_print_calibration(&bno_055);
}

bool sensors_wakeup(void) {
    if (!bno055_wakeup(&bno_055)) {
        ESP_LOGE(TAG, "Couldn't reactivate BNO-055.");
        return false;
    }
    vTaskDelay(50);

    // In some cases documented in the Bosch Sensortec Community forum, the BNO
    // lost its calibration during long suspend modes. Re-push our calibration
    // profile if it lost calibration.
    if (!bno055_is_calibrated(&bno_055)) {
        bno055_push_calibration(&bno_055);
        // sensors_calibrate();
    }

    return true;
}

bool sensors_enable_lowpower(void) {
    if (!bno055_lowpower_enable(&bno_055)) {
        ESP_LOGE(TAG, "Couldn't put BNO-055 in low-power mode.");
        return false;
    }
    return true;
}

bool sensors_start_sampling(void) {
    if (!sensors_wakeup())
        return false;

    return (xTimerStart(bno055_timer, 10) == pdPASS);
}

bool sensors_stop_sampling(void) {
    if (!sensors_enable_lowpower())
        return false;
    return (xTimerStop(bno055_timer, 10) == pdPASS);
}

void timer_callback(TimerHandle_t timer) {
    // note that this isn't an ISR, but we still shouldn't block the timer task
    if (timer == bno055_timer && sensor_task != NULL) {
        xTaskNotify(sensor_task, 1, eSetBits); // notify with anything != 0
    }
}