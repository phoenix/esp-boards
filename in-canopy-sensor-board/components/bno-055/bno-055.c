/**
 ******************************************************************************
 * @file           bno055.c
 * @brief          Implements the sensor communication with Bosch SensorTec's
 *                      BNO055, based on the Bosch Reference Implementation.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <stdbool.h>
#include <string.h>

#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "bno-055-bosch.h"
#include "bno-055.h"
#include "i2c-bus.h"

/* Private variables -------------------------------------------------------- */

/** Tag for the ESP-LOG framework. */
static const char *TAG = "bno-055";

static i2c_port_t active_i2c_bus = 0;

/* Faulty sensor value detection -------------------------------------------- */

//! Minimum value for the heading that is accepted: >= 0 radians <==>  > -1
#define BNO055_MIN_QUAT_COMP (-16384)
#define BNO055_MAX_QUAT_COMP (+16384)

//! Minimum linear acceleration that is accepted: -160 m/s^2
#define BNO055_MIN_ACCEL (-160 * 100)
//! Maximum linear acceleration that is accepted: +160 m/s^2
#define BNO055_MAX_ACCEL (160 * 100)

//! Minimum angular velocity that is accepted: -35 rad/s
#define BNO055_MIN_RPS (-31500)
//! Maximum angular velocity that is accepted: 35 rad/s
#define BNO055_MAX_RPS (31500)

/* Low-level drivers for BNO-055 I2C communication -------------------------- */

/** I2C bus write function for the BNO055 driver */
int8_t bno055_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data,
                    uint8_t cnt) {
    return i2c_master_write_slave_reg(dev_addr, reg_addr, data, cnt,
                                      active_i2c_bus);
}

/** I2C bus read function for the BNO055 driver */
int8_t bno055_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data,
                   uint8_t cnt) {
    return i2c_master_read_slave_reg(dev_addr, reg_addr, data, cnt,
                                     active_i2c_bus);
}

/** Millisecond delay function for the BNO055 driver */
void bno055_delay(uint32_t delay) {
    vTaskDelay(delay / portTICK_PERIOD_MS);
}

/* BNO-055 Setup ------------------------------------------------------------ */

bool bno055_setup(BNO055_Handle *bno055) {
    assert(bno055 != NULL);

    // Set up the pin connected to BNO's NRST as open drain output with pull-up
    bno055->rst_pin_setup = false;
    gpio_config_t nrst_cfg = {
        .pin_bit_mask = 1 << bno055->reset_pin,
        .pull_up_en = GPIO_PULLUP_ENABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .mode = GPIO_MODE_OUTPUT_OD,
        .intr_type = GPIO_INTR_DISABLE,
    };

    if (gpio_config(&nrst_cfg) == ESP_ERR_INVALID_ARG) {
        ESP_LOGE(TAG, "Invalid GPIO configuration for BNO-055 reset pin (NRST)."
                      " Check menuconfig.");
        return false;
    }
    bno055->rst_pin_setup = true;

    return bno055_reset(bno055);
}

bool bno055_reset(BNO055_Handle *bno055) {
    assert(bno055 != NULL);

    // make sure that the NRST gpio has been properly set up
    if (!bno055->rst_pin_setup)
        return false;

    // hold down NRST pin for at least 20ns, so round it up to 1us wait (p.
    // 18)
    gpio_set_level(bno055->reset_pin, 0);
    ets_delay_us(1);
    gpio_set_level(bno055->reset_pin, 1);

    // wait 650ms for BNO to start up (p. 13 datasheet)
    vTaskDelay(650 / portTICK_RATE_MS);

    // This is the BNO055 initialization struct that helps the Bosch API
    // communicate with our implementation.
    // IMPORTANT: Do not remove the "static" keyword, because this structs
    // needs
    //  to remain in memory. The Bosch API keeps the pointer handed to it,
    //  and the API tries to call the bus_write/bus_read pointers without
    //  there being any, expect crashes with a backtrace ending in something
    //  like "0xfffffffd"
    memset(&(bno055->bno_priv), 0, sizeof(struct bno055_t)); // zero out
    bno055->bno_priv.bus_write = &bno055_write;
    bno055->bno_priv.bus_read = &bno055_read;
    bno055->bno_priv.delay_msec = &bno055_delay;
    bno055->bno_priv.dev_addr = BNO055_I2C_ADDR1;

    int32_t status;
    active_i2c_bus = bno055->i2c_port_num;
    status = bno055_init(&(bno055->bno_priv));

    if (status != 0) {
        ESP_LOGE(TAG, "BNO-055 init failed. Return status: (%i)", status);
        return false;
    }

    // Perform self test of the BNO's gyro, magnetometer and accelerometer
    if (bno055_set_selftest(BNO055_BIT_ENABLE) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "BNO-055 self-test could not be started!");
        return false;
    } else {
        // wait 400ms for the self-test to be performed
        vTaskDelay(400 / portTICK_PERIOD_MS);

        uint8_t selftest_accel = 0;
        uint8_t selftest_gyro = 0;
        uint8_t selftest_mag = 0;
        uint8_t selftest_mcu = 0;

        bno055_get_selftest_accel(&selftest_accel);
        bno055_get_selftest_gyro(&selftest_gyro);
        bno055_get_selftest_mag(&selftest_mag);
        bno055_get_selftest_mcu(&selftest_mcu);

        // all get set to 1 if the selftest was successful
        if (selftest_accel && selftest_gyro && selftest_mag && selftest_mcu) {
            ESP_LOGI(TAG, "BNO-055 self-test passed.");
        } else {
            ESP_LOGE(TAG, "BNO-055 self-test failed.");
            return false;
        }
    }

    // Set to active power mode to start
    bno055_set_power_mode(BNO055_POWER_MODE_NORMAL);

    uint16_t sw_version;
    bno055_read_sw_rev_id(&sw_version);
    ESP_LOGI(TAG, "BNO-055 software version is %X", sw_version);

    // Set units for the measurements
    if ((status = bno055_set_accel_unit(BNO055_ACCEL_UNIT_MSQ)) !=
        BNO055_SUCCESS) {
        ESP_LOGE(
            TAG,
            "Could not set linear acceleration unit to m/s^2. Status: (%i)",
            status);
    }
    if ((status = bno055_set_gyro_unit(BNO055_GYRO_UNIT_RPS)) !=
        BNO055_SUCCESS) {
        ESP_LOGE(TAG,
                 "Could not set angular velocity unit to rad/s. Status: (%i)",
                 status);
    }
    if ((status = bno055_set_euler_unit(BNO055_EULER_UNIT_RAD)) !=
        BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Could not set euler angles unit to rad. Status: (%i)",
                 status);
    }

    // Remap axes: The sensor's (+X)(+Y)(+Z) corresponds to our (+X)(-Z)(+Y)
    if ((status = bno055_set_axis_remap_value(BNO055_REMAP_Y_Z)) != 0) {
        ESP_LOGE(TAG, "Could not remap Y-Z axes!");
    }

    if ((status = bno055_set_remap_y_sign(BNO055_REMAP_AXIS_POSITIVE)) != 0) {
        ESP_LOGE(TAG, "Could not set positive y axis sign!");
    }

    if ((status = bno055_set_remap_z_sign(BNO055_REMAP_AXIS_NEGATIVE)) != 0) {
        ESP_LOGE(TAG, "Could not set negative z axis sign!");
    }

    // set operation mode to nine-dof sensor fusion
    if ((status = bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF)) != 0) {
        ESP_LOGE(TAG, "Error setting BNO-055 into NDOF mode. Status: (%i)",
                 status);
    }

    // setting the ranges for the accelerometer etc. isn't possible, since
    // it's managed by the 9-DoF sensor fusion algorithm

    return true;
}

bool bno055_lowpower_enable(BNO055_Handle *bno055) {
    active_i2c_bus = bno055->i2c_port_num;
    bno055_set_operation_mode(BNO055_OPERATION_MODE_CONFIG);
    return bno055_set_power_mode(BNO055_POWER_MODE_SUSPEND) == BNO055_SUCCESS;
}

bool bno055_wakeup(BNO055_Handle *bno055) {
    active_i2c_bus = bno055->i2c_port_num;
    bno055_set_power_mode(BNO055_POWER_MODE_NORMAL);
    return bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF) ==
           BNO055_SUCCESS;
}

bool bno055_sample(BNO055_Handle *bno055, struct BNO055_Readout *dest) {
    assert(bno055 != NULL);
    assert(dest != NULL);

    active_i2c_bus = bno055->i2c_port_num;

    struct bno055_linear_accel_t accel = {0};
    struct bno055_gyro_t angvel = {0};
    struct bno055_quaternion_t qtrn = {0};

    if (bno055_read_linear_accel_xyz(&accel) == BNO055_ERROR) {
        ESP_LOGE(TAG, "Could not read acceleration!");
        return false;
    }
    if (bno055_read_gyro_xyz(&angvel) == BNO055_ERROR) {
        ESP_LOGE(TAG, "Could not read angular velocity!");
        return false;
    }
    if (bno055_read_quaternion_wxyz(&qtrn) == BNO055_ERROR) {
        ESP_LOGE(TAG, "Failed to read quaternion output.");
        return false;
    }

    // Check whether all values make sense
    if (!(accel.x > BNO055_MIN_ACCEL && accel.x < BNO055_MAX_ACCEL &&
          accel.y > BNO055_MIN_ACCEL && accel.y < BNO055_MAX_ACCEL &&
          accel.z > BNO055_MIN_ACCEL && accel.z < BNO055_MAX_ACCEL)) {
        ESP_LOGE(TAG, "Faulty accel reading of BNO-055 detected!");
        return false;
    }

    if (!(angvel.x > BNO055_MIN_RPS && angvel.x < BNO055_MAX_RPS &&
          angvel.y > BNO055_MIN_RPS && angvel.y < BNO055_MAX_RPS &&
          angvel.z > BNO055_MIN_RPS && angvel.z < BNO055_MAX_RPS)) {
        ESP_LOGE(TAG, "Faulty gyro reading of BNO-055 detected!");
        return false;
    }

    if (!(qtrn.x > BNO055_MIN_QUAT_COMP && qtrn.x < BNO055_MAX_QUAT_COMP &&
          qtrn.y > BNO055_MIN_QUAT_COMP && qtrn.y < BNO055_MAX_QUAT_COMP &&
          qtrn.z > BNO055_MIN_QUAT_COMP && qtrn.z < BNO055_MAX_QUAT_COMP &&
          qtrn.w > BNO055_MIN_QUAT_COMP && qtrn.w < BNO055_MAX_QUAT_COMP)) {
        // One of the sensor values is out of bounds
        ESP_LOGE(TAG, "Faulty quaternion reading of BNO-055 detected!");
        return false;
    } else if (qtrn.x == 0 && qtrn.y == 0 && qtrn.z == 0 && qtrn.w == 0) {
        // Read a zero quaternion, a great indicator that something is wrong
        ESP_LOGE(TAG, "Zero quaternion reading of BNO-055 detected!");
        return false;
    }

    memcpy(dest->lin_acc.array, &accel, 3 * sizeof(int16_t));
    memcpy(dest->ang_vel.array, &angvel, 3 * sizeof(int16_t));
    memcpy(dest->qtr_att.array, &qtrn, 4 * sizeof(int16_t));
    dest->tick = (uint32_t)xTaskGetTickCount();

    ESP_LOGV(TAG,
             "Acceleration: x: %7i, y: %7i, z: %7i"
             "; p: %7i, q: %7i, r: %7i; x = %7i, y = %7i, z = %7i, w = %7i",
             dest->lin_acc.x, dest->lin_acc.y, dest->lin_acc.z, dest->ang_vel.p,
             dest->ang_vel.q, dest->ang_vel.r, dest->qtr_att.x, dest->qtr_att.y,
             dest->qtr_att.z, dest->qtr_att.w);

    return true;
}

bool bno055_is_calibrated(BNO055_Handle *bno055) {

    assert(bno055 != NULL);

    active_i2c_bus = bno055->i2c_port_num;
    uint8_t magn_calib_stat = 0;
    uint8_t gyro_calib_stat = 0;
    uint8_t accl_calib_stat = 0;

    // As per recommendation by Bosch Sensortec, don't check the system
    // calibration status, because there is a bug in firmware 3.11. Instead,
    // if gyroscope and magnetometer are 3/3, the system can be considered
    // "calibrated"

    if ((bno055_get_mag_calib_stat(&magn_calib_stat) != BNO055_SUCCESS) ||
        (bno055_get_gyro_calib_stat(&gyro_calib_stat) != BNO055_SUCCESS) ||
        (bno055_get_accel_calib_stat(&accl_calib_stat) != BNO055_SUCCESS)) {
        ESP_LOGE(TAG, "Failed to receive the calib status of one sensor.");
        return false;
    }

    ESP_LOGI(TAG,
             "BNO-055 Calibration Status. %u/3 (accel), %u/3 (gyro), %u/3 "
             "(magneto)",
             accl_calib_stat, gyro_calib_stat, magn_calib_stat);

    return magn_calib_stat == 3 && gyro_calib_stat == 3;
    // Source:
    // https://community.bosch-sensortec.com/t5/MEMS-sensors-forum/BNO055-Calibration-Staus-not-stable/m-p/8379/highlight/true#M926
}

bool bno055_push_calibration(BNO055_Handle *bno055) {
    assert(bno055 != NULL);
    active_i2c_bus = bno055->i2c_port_num;
    // all these values can be modified through `idf.py menuconfig`
    struct bno055_accel_offset_t accel_offset = {
        .x = CONFIG_BNO_ACCEL_OFFSET_X,
        .y = CONFIG_BNO_ACCEL_OFFSET_Y,
        .z = CONFIG_BNO_ACCEL_OFFSET_Z,
        .r = CONFIG_BNO_ACCEL_OFFSET_R,
    };

    struct bno055_gyro_offset_t gyro_offset = {
        .x = CONFIG_BNO_GYRO_OFFSET_X,
        .y = CONFIG_BNO_GYRO_OFFSET_Y,
        .z = CONFIG_BNO_GYRO_OFFSET_Z,
    };

    struct bno055_mag_offset_t mag_offset = {
        .x = CONFIG_BNO_MAG_OFFSET_X,
        .y = CONFIG_BNO_MAG_OFFSET_Y,
        .z = CONFIG_BNO_MAG_OFFSET_Z,
        .r = CONFIG_BNO_MAG_OFFSET_R,
    };

    bool success = true;
    // If these fail, fail gracefully. Why? Because the sensor will be able
    // to calibrate itself, it will just take some time
    if (bno055_write_accel_offset(&accel_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't write accel offset!");
        success = false;
    }

    if (bno055_write_gyro_offset(&gyro_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't write gyro offset!");
        success = false;
    }

    if (bno055_write_mag_offset(&mag_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't write mag offset!");
        success = false;
    }

    if (success)
        ESP_LOGI(TAG, "Wrote these offsets: ");
    else
        ESP_LOGI(TAG, "Didn't write these offsets:");

    ESP_LOGI(TAG,
             "A: .x=%i, .y=%i, .z = %i, .r=%i, G: .x=%i, .y=%i, .z=%i, "
             "M: .x=%i, .y=%i, .z=%i, .r=%i",
             accel_offset.x, accel_offset.y, accel_offset.z, accel_offset.r,
             gyro_offset.x, gyro_offset.y, gyro_offset.z, mag_offset.x,
             mag_offset.y, mag_offset.z, mag_offset.r);

    return success;
}

/**
 * @brief Prints the current calibration values for the BNO055 to a serial
 * port.
 *
 * This function gets the calibration values for accelerometer, gyroscope
 * and magnetometer and writes them to the serial monitor.
 */
bool bno055_print_calibration(BNO055_Handle *bno055) {
    assert(bno055 != NULL);

    // Current offsets will be read into these structs
    struct bno055_accel_offset_t accel_offset = {0};
    struct bno055_gyro_offset_t gyro_offset = {0};
    struct bno055_mag_offset_t mag_offset = {0};

    bool success = true;
    if (bno055_read_accel_offset(&accel_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't read accel offset!");
        success = false;
    }

    if (bno055_read_gyro_offset(&gyro_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't read gyro offset!");
        success = false;
    }

    if (bno055_read_mag_offset(&mag_offset) != BNO055_SUCCESS) {
        ESP_LOGE(TAG, "Couldn't read mag offset!");
        success = false;
    }

    if (success) {
        ESP_LOGI(TAG, "Read those these offsets: ");
        ESP_LOGI(TAG,
                 "A: .x=%i, .y=%i, .z = %i, .r=%i, G: .x=%i, .y=%i, .z=%i, "
                 "M: .x=%i, .y=%i, .z=%i, .r=%i",
                 accel_offset.x, accel_offset.y, accel_offset.z, accel_offset.r,
                 gyro_offset.x, gyro_offset.y, gyro_offset.z, mag_offset.x,
                 mag_offset.y, mag_offset.z, mag_offset.r);
    } else {
        ESP_LOGI(TAG, "Couldn't read offsets.");
    }

    return success;
}
