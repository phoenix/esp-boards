# In-canopy sensor boards

This repository contains the code that is running on the in-canopy sensor boards for the Guided Recovery focus project Phoenix.

To quickly judge the status of the board during operation, observe the blinking rate of the *red LED* next to the USB port:

* 1 per second (`Initial`): initialized, not calibrated
* Continous light on: Calibration in progress…
* 2 per second (`Calibrated`): Sensors set up
* None per second (`Light Sleep`): Sleeping
* 3 per second (`Active`): actively sampling and transmitting
* 10 per second: fatal error state

## Overview

### Hardware
An in-canopy sensor board is run by an ESP32 microcontroller. In our project, we used the [Huzzah32](https://www.adafruit.com/product/3405) variant developed by Adafruit because they feature a built-in LDO to allow a direct connection to a LiPo battery.

Via I2C, the sensors are connected; both sensors are on their separate, CTOS breakout boards.

## Software Architecture

The ESP-IDF framework builds on `cmake`, and encourages splitting up the code over several 'modules'. For every module, there is a folder in `components/` with a `CMakeLists.txt` file containing the build instructions.

### ICSB Modules

These modules run only on the ICSB. Find the shared components in the top-level `README.md` of the repository. Many of the modules contain functions that will run as [Tasks](#Tasks).

| Module Name        | What does it do?                                             | Ready for Review? | Reviewed / unchanged? |
| ------------------ | ------------------------------------------------------------ | ----------------- | --------------------- |
| `peripherals`      | Set up the GPIO pins used for I2C communication. Provides low-layer methods for I2C communication for the BNO-055 sensor. | ✅                 |                       |
| `sensors`          | Task that samples all the sensors at their predefined rates. | ✅                 |                       |
| `status-indicator` | Blinky task that indicates the current ICSB state visually.  | ✅                 |                       |
| `status-reporting` | Task that sends the current ICSB state to the main board at a predefined rate. | ✅                 |                       |
| `main`             | Finite State Machine task. Manages the different states of the board. | ✅                 |                       |

### Tasks

#### Task overview

The In-Canopy Sensor Board runs the following tasks concurrently:

| Task           | Description                                                  | Priority | Stack Size (words)  |
| -------------- | ------------------------------------------------------------ | -------- | ------------------- |
| `main`         | Finite State Machine, management of other tasks              | 5        | unknown             |
| `sensor`       | Sample the sensors, hand over to `packets` for transmission  | 4        | 1000 (not verified) |
| `packets`      | Manage the packet transmission / receival and hand-off to `main` | 3        | 750 (not verified) |
| `status_blink` | Blink the status LED.                                        | 2        | 500 (~236 free) |
| `status-report` | Send a status packet to the main board via WiFi. | 2 | 256 |

#### Main task

The main task is contained within `main.c`: It runs the finite state machine and creates all the other tasks. It runs at the highest priority among the user tasks to ensure that FSM transitions are triggered as quickly as possible.

The entry point of the main task is the `app_main()` function, which is called by the ESP-IDF toolchain.

### Finite State Machine

![Finite State Machine Graph](FSM-ICSB.png "Finite State Machine Graph")

#### Sleep Mode

The sleep mode is *light sleeping* with disabled RF module, i.e. no WiFi enabled. The ESP will power down sensors and the WiFi module and sleep for 60 seconds, waking up from timer. After 60 seconds, it enables WiFi for 2 seconds and listens for any commands received via ESP-NOW. If no "Activate" command was received, snooze & repeat.

## Reference Manual

### Nominal behaviour

Upon connecting the board to power, it will set up the GPIO ports and perform an I2C selftest to see whether the BNO-055 and the BMP-388 sensors are connected. Furthermore, hardware self checks are executed and evaluated for both sensors.

The ICSB will then wait for remote commands to be sent to it.

### Remote Commands 

The ICSB accepts all packets sent to it via ESP-NOW. If `.type` of the packet is `CommandPacket`, the payload will be interpreted as one of the following commands:

* `Activate`: 0x01;
* `Sleep`: 0x02;
* `Calibrate`: 0x04;
* `Restart`: 0x128.

### Pinout

The nominal pinout of the board (if the `sdkconfig` is not changed after cloning) is as follows:

* GPIO 22 (serial clock): connect to `SCL` of BNO-055 breakout and BMP-388 breakout
* GPIO 23 (serial data): connect to `SDA` of BNO-055 breakout and BMP-388 breakout
* GPIO 21 (reset pin): connect to `RST` of BNO-055 breakout
* `3V`: 3.3V, connect to `3V` of BNO-055 breakout
* `GND`: ground reference, connect to `GND` of BNO-055 breakout