/**
 ******************************************************************************
 * @file           main.h
 * @brief          Definitions for the main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "packets.h"

/* Task management ---------------------------------------------------------- */

#define TASK_MAIN_PRIORITY 5 ///< Priority of the main program body (FSM)
#define TASK_SENS_PRIORITY 4 ///< Priority of the sensor polling task
#define TASK_PCKT_PRIORITY 3 ///< Priority of the WiFi transmission task
#define TASK_BLNK_PRIORITY 2 ///< Priority of the LED blinking task.
#define TASK_STAT_PRIORITY 2 ///< Priority of the status sending task.

/* Global variables --------------------------------------------------------- */

/** The location that the current ICSB is at: left or right */
extern enum Location icsb_location;

/**
 * Task handle for the main task.
 *
 * This handle can be used to notify the main task of a finite state machine
 *  transition that should occur.
 */
extern TaskHandle_t main_task;

/* Finite state machine ----------------------------------------------------- */

/**
 * @brief An enum containing the states the in-canopy sensor boards could be in.
 *
 * The in-canopy sensor boards will start up and try to make a connection to the
 * main board ESP32 microcontroller. Then, they will wait for further input
 * commands.
 * After configuration, the system can dynamically switch between `Active` and
 * `Sleeping` modes to preserve battery power.
 */
enum SystemState {
    Stuck =
        0,   ///< A transition that can't be accepted leads to the stuck state.
    Initial, ///< The initial state the system is put in.
    Calibrated, ///< The peripherals all have successfully been calibrated.
    Active,     ///< Active sensor sampling and sending the samples over WiFi.
    Sleeping    ///< The system is in sleep mode and will periodically wake up.
};

/**
 * @brief An enum containing the possible external inputs to the finite state
 *    manager.
 *
 * Keep in mind that transitions are only valid for some states (see allowed
 * states in the implementation file.
 * To signal a needed transition to the finite state manager, set the event flag
 * to the system input enum value.
 */
enum SystemInput {
    None = 0x00,      ///< Null input.
    Calibrate = 0x01, ///< Configure all peripherals.
    Activate = 0x02,  ///< Activate sensors and start transmitting.
    Sleep = 0x04      ///< Set the system to sleep mode.
};

/**
 * @brief A struct declaring a possible transition of a system from one state to
 *      another.
 *
 * A transition consists of two states and one input: The initial state, an
 * input that can be processed in that state and the next state the system will
 * be in. The handler designates the function that will execute the transition.
 */
struct SystemTransition {
    enum SystemState initial_state; ///< The state the system should be in.
    enum SystemState final_state; ///< The state that the system should move to.
    enum SystemInput input;       ///< The correspondig system input.
    void (*handler)(void);        ///< The handler implementing the transition.
};

/* Error handling ----------------------------------------------------------- */

/**
 * Handler for when a fatal error occurred in the system. Delay forever.
 */
void fatal_error(void) __attribute__((noreturn));

/* State management --------------------------------------------------------- */

/**
 * Get the current state of the system.
 * @returns The current system state.
 */
enum SystemState get_system_state(void);

/* Packet handling ---------------------------------------------------------- */

/**
 * Packet reception handler for the in-canopy sensor board.
 *
 * This reception handler is called by the packet handling task upon receiving
 * a packet addressed to this microcontroller. It runs on the computational time
 * of the wireless task. This is fine, because the in-canopy boards don't often
 * receive remote commands. In addition, these commands (active / sleep) are not
 * milli-second critical.
 * @param packet The packet that was received.
 */
void packet_received(Packet *packet);

#endif /* __MAIN_H__ */
