/**
 *******************************************************************************
 * @file           i2c-bus.c
 * @brief          Implements the communication with I2C buses for the ESP.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "i2c-bus.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"

/* I2C configuration macros for better readability -------------------------- */

#define I2C_STANDARD_MODE         100000
#define I2C_FAST_MODE             400000
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT                 I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                  I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN              0x1 /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS             0x0 /*!< I2C master will not check ack from slave */
#define ACK_VAL                   0x0 /*!< I2C ack value */
#define NACK_VAL                  0x1 /*!< I2C nack value */
#define LAST_NACK                 0x2 /*!< I2C nack after last byte */

/* Private variables -------------------------------------------------------- */

static const char *TAG = "i2c-bus";

/* Public function implementation ------------------------------------------- */

bool i2c_bus_setup(i2c_port_t port, gpio_num_t scl_pin, gpio_num_t sda_pin) {

    // default setup: 400kHz (fast mode)
    i2c_config_t i2c_config = {.mode = I2C_MODE_MASTER,
                               .sda_io_num = sda_pin,
                               .scl_io_num = scl_pin,
                               .sda_pullup_en = GPIO_PULLUP_DISABLE,
                               .scl_pullup_en = GPIO_PULLUP_DISABLE,
                               .master.clk_speed = I2C_FAST_MODE};

    esp_err_t err = ESP_OK;

    err = i2c_driver_install(port, I2C_MODE_MASTER, I2C_MASTER_RX_BUF_DISABLE,
                             I2C_MASTER_TX_BUF_DISABLE, 0);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to install I2C driver! Reason: (%i)", err);
        return false;
    }

    // install driver without TX/RX buffer lengths (only for slave mode) and no
    // interrupts

    err = i2c_param_config(port, &i2c_config);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to configure I2C port! Reason: (%i)", err);
        return false;
    }

    err = i2c_set_timeout(port, 0xFFFFF);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to set timeout for I2C port! Reason: (%i)", err);
        return false;
    }

    int timeout = 0;
    i2c_get_timeout(port, &timeout);
    ESP_LOGD(TAG, "Current I2C timeout is %i", timeout);

    return true;
}

int8_t i2c_master_read_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                 uint8_t *data, uint8_t cnt, i2c_port_t port) {

    if (cnt == 0) {
        return ESP_OK;
    }
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send start condition
    i2c_master_start(cmd);

    // Send device address and write bit, check for ACK
    i2c_master_write_byte(cmd, (dev_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
    // Send register address, check for ACK
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);

    // Send repeated start
    i2c_master_start(cmd);
    // Send device address and read bit, check for ACK
    i2c_master_write_byte(cmd, (dev_addr << 1) | READ_BIT, ACK_CHECK_EN);

    // Read cnt bytes, sending ACK after each one, but NACK on last one
    i2c_master_read(cmd, data, cnt, LAST_NACK);

    // Send stop condition
    i2c_master_stop(cmd);

    // done composing the I2C command, send, delete and return status
    // wait for 50ms max
    esp_err_t ret = i2c_master_cmd_begin(port, cmd, 50 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    if (ret != ESP_OK) {
        ESP_LOGE(TAG,
                 "Reading from device at %#x, register %#x, %i bytes, failed.",
                 dev_addr, reg_addr, cnt);
        ESP_LOGD(TAG, "Reason for failure (I2C return status): %i.", ret);
    }

    return ret;
}

int8_t i2c_master_write_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                  uint8_t *data, uint8_t cnt, i2c_port_t port) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send start condition
    i2c_master_start(cmd);

    // Send device address and write bit, check for ACK
    i2c_master_write_byte(cmd, (dev_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
    // Send register address, check for ACK
    i2c_master_write_byte(cmd, reg_addr, ACK_CHECK_EN);

    // Send data
    i2c_master_write(cmd, data, cnt, ACK_CHECK_EN);

    // Send stop
    i2c_master_stop(cmd);

    // done composing the I2C command, send, delete and return status
    // wait for 50ms max
    esp_err_t ret = i2c_master_cmd_begin(port, cmd, 50 / portTICK_PERIOD_MS);

    if (ret != ESP_OK) {
        ESP_LOGE(TAG,
                 "Writing to device at %#x, register %#x, %i bytes, failed.",
                 dev_addr, reg_addr, cnt);
        ESP_LOGD(TAG, "Reason for failure (I2C return status): %i.", ret);
    }

    i2c_cmd_link_delete(cmd);

    return ret;
}

bool i2c_test_device_connection(uint8_t dev_addr, i2c_port_t port) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (dev_addr << 1) | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(port, cmd, 1000 / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);
    return ret == ESP_OK;
}