void func(void);
/**
 *******************************************************************************
 * @file           i2c-bus.h
 * @brief          Interface for setting up/using the I2C bus on the ESP32.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdint.h>

#include "driver/gpio.h"
#include "driver/i2c.h"

/**
 * @brief Set up the specified I2C bus.
 *
 * The I2C port is configured as fast mode (400 kHz) without pull-ups enabled,
 * because the breakout boards usually feature their own pull-ups.
 * @param[in] port The port number of the I2C bus that should be set up.
 * @returns If the bus setup was successful.
 */
bool i2c_bus_setup(i2c_port_t port, gpio_num_t scl_pin, gpio_num_t sda_pin);

/**
 * Simple I2C register read method.
 *
 * Performs the multi-byte read sequence that goes as follows:
 * -----------------------------------------------------------------------------
 * | MASTER | START | dev_addr + wr |     | reg_addr |     | START | dev_addr +
 * -----------------------------------------------------------------------------
 * | SLAVE  |       |               | ACK |          | ACK |       |
 * =============================================================================
 * rd |     |      | ACK | ...  | NACK | STOP |
 *    | ACK | DATA | ... | DATA |      |      |
 * -----------------------------------------------------------------------------
 * @param[in] dev_addr The 7-bit address of the device, non-shifted
 * @param[in] reg_addr The register address to read
 * @param[out] data A pointer to an array, where the data can be written.
 * @param[in] cnt The number of bytes to read.
 * @param[in] port The I2C port to use.
 * @returns Non-zero on error.
 */
int8_t i2c_master_read_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                 uint8_t *data, uint8_t cnt, i2c_port_t port);

/**
 * Simple I2C register write method.
 *
 * Performs the multi-byte write sequence that goes as follows:
 * -----------------------------------------------------------------------------
 * | MASTER | START | dev_addr + wr |     | reg_addr |     | DATA |     | ... |
 * -----------------------------------------------------------------------------
 * | SLAVE  |       |               | ACK |          | ACK |      | ACK |     |
 * =============================================================================
 *     | STOP |
 * ... |      |
 * -----------------------------------------------------------------------------
 * @param[in] dev_addr The 7-bit address of the device, non-shifted
 * @param[in] reg_addr The register address to start writing to.
 * @param[out] data A pointer to an array, where the data to be written is.
 * @param[in] cnt The number of bytes to write.
 * @param[in] port The I2C port to use.
 * @returns Non-zero on error.
 */
int8_t i2c_master_write_slave_reg(uint8_t dev_addr, uint8_t reg_addr,
                                  uint8_t *data, uint8_t cnt, i2c_port_t port);

/**
 * @brief Test whether a device on the I2C port is responsive.
 *
 * This method sends out the device address on the I2C bus specified and checks
 * whether the device responds with ACK.
 * @param[in] dev_addr The 7-bit address of the device, non-shifted
 * @param[in] port The I2C port to use.
 * @return Whether the device responded.
 */
bool i2c_test_device_connection(uint8_t dev_addr, i2c_port_t port);