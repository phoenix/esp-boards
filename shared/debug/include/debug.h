/**
 *******************************************************************************
 * @file           debug.h
 * @brief          Declares functions that are useful for debugging.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

// Global DEBUG flag
#define DEBUG 0

#include <stdint.h>

/**
 * Print a debug line to serial if and only if DEBUG is enabled. No LF needed.
 * @param fmt The standard printf format.
 * @see Epic source:
 * https://stackoverflow.com/questions/1644868/define-macro-for-debug-printing-in-c
 */
#define debug_printf(fmt, ...)                                                 \
    do {                                                                       \
        if (DEBUG)                                                             \
            printf("%s:%d:%s(): " fmt "\n", __FILE__, __LINE__, __func__,      \
                   ##__VA_ARGS__);                                             \
    } while (0)

/**
 * Start the clock to time execution time in microseconds.
 * NOT THREADSAFE!
 */
void clock_start(void);

/**
 * Stop the clock and get number of microseconds elapsed.
 * NOT THREADSAFE!
 */
uint_fast32_t clock_stop(void);
