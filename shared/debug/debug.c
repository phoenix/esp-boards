/**
 *******************************************************************************
 * @file           debug.c
 * @brief          Implementations of debugging functionality.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "debug.h"
#include <sys/time.h>

static struct timeval start_time = {0};

void clock_start() {
    gettimeofday(&start_time, NULL);
}

uint_fast32_t clock_stop() {
    static struct timeval end_time;
    gettimeofday(&end_time, NULL);

    uint_fast32_t elapsed_u_sec = (end_time.tv_sec - start_time.tv_sec) * 1e6 +
                                  (end_time.tv_usec - start_time.tv_usec);
    return elapsed_u_sec;
}
