/**
 *******************************************************************************
 * @file           serialization.h
 * @brief          Helper for serialization of data payloads.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include "data.h"
#include "packets.h"

/**
 * @brief Serialize a data payload into an unsigned byte buffer.
 * @pre pl must not be NULL and buf must contain at least 19 bytes
 * @param pl The payload to serialize.
 * @param buf The buffer (>= 25 bytes) to write the serialized entry into.
 */
void serialize_data_payload(struct DataPayload *pl, uint8_t *buf);

/**
 * @pre Deserialize a data payload from an unsigned byte buffer.
 * @pre pl Must not be NULL and buf must contain 25 bytes of data payload.
 * @param buf The buffer to deserialize.
 * @param pl The memory location where the deserialized data is written.
 */
void deserialize_data_payload(uint8_t *buf, struct DataPayload *pl);