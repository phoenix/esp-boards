/**
 *******************************************************************************
 * @file           packets.c
 * @brief          Main program body
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "debug.h"
#include "packets.h"

/* Private variables -------------------------------------------------------- */
static const char *TAG = "packets";

/* Private functions -------------------------------------------------------- */

/**
 * Process a packet.
 *
 * The processing is done on the packet task.
 * @param packet A pointer to the packet to process.
 * @returns Whether the processing was successful.
 */
int packet_process(Packet *packet);

/* RTOS functionality ------------------------------------------------------- */

QueueHandle_t packet_queue;

void packet_task_entry(void *arg) {
    assert(arg != NULL);
    struct PacketTaskConfig *config = (struct PacketTaskConfig *)arg;

    uint_fast8_t num_tx = config->num_transmission_handlers;
    enum Location here = config->here;

    ESP_LOGD(TAG, "Configuration contains %i transmission handlers.", num_tx);

    struct PacketTransmissionHandler *tx_hdls =
        malloc(num_tx * sizeof(struct PacketTransmissionHandler));
    memcpy(tx_hdls, config->tx_handlers,
           num_tx * sizeof(struct PacketTransmissionHandler));

    void (*reception_handler)(Packet * packet) = config->reception_handler;

    if (num_tx == 0 || reception_handler == NULL) {
        ESP_LOGI(TAG, "Number of reception or transmission handlers is zero. "
                      " This doesn't seem very useful.");
    }

    packet_queue = xQueueCreate(30, sizeof(Packet));
    assert(packet_queue != NULL);

    // start handling packets

    Packet packet;
    while (1) {
        if (xQueueReceive(packet_queue, &packet, portMAX_DELAY) == pdPASS) {
            UBaseType_t available_spaces = uxQueueSpacesAvailable(packet_queue);
            ESP_LOGV(TAG, "%i packets available in queue!", available_spaces);
            if (packet.destination == here) {
                // packet received, look for handler for this sender
                if (reception_handler != NULL) {
                    reception_handler(&packet);
                } else {
                    ESP_LOGD(TAG, "Received packet, but no reception handler "
                                  "configured.");
                }
                continue;
            }

            // else: send packet. loop through transmission handlers to find
            // suitable
            uint_fast8_t found_tx = 0;
            for (uint_fast8_t i = 0; i < num_tx; i++) {
                ESP_LOGV(TAG,
                         "Transmission handler's destination is %i, packet "
                         "destination is %i",
                         tx_hdls[i].destination, packet.destination);
                if (tx_hdls[i].destination == packet.destination) {
                    if (tx_hdls[i].transmitter(&packet) != 0)
                        ESP_LOGD(TAG,
                                 "Transmission handler returned an error.");

                    found_tx = 1;
                    break;
                }
            }

            if (found_tx == 0) {
                ESP_LOGD(
                    TAG,
                    "No suitable transmission handler found. Drop packet...");
                free(packet.payload);
            }

        } else {
            ESP_LOGE(TAG, "Error retrieving item from queue.");
        }
    }
}

int packet_add_to_queue(Packet *packet) {
    assert(packet_queue != NULL);
    assert(packet != NULL);

    BaseType_t ret;
    if (packet->priority == PriorityHigh) {
        ESP_LOGD(TAG, "Packet has priority. Inserted at front of queue.");
        ret = xQueueSendToFront(packet_queue, packet, 100);
    } else {
        ret = xQueueSendToBack(packet_queue, packet, 100);
    }

    if (ret == errQUEUE_FULL) {
        ESP_LOGE(TAG, "Couldn't add packet to queue because it is full!");
        return -1;
    }

    return 0;
}
