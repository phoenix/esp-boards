/**
 *******************************************************************************
 * @file           data.h
 * @brief          Declares the data types for the data being collected.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Raw register data -------------------------------------------------------- */

// This section represents one physical quantity measured by a sensor in its raw
// register form. These do not have units, but must first be converted using the
// methods provided in this header file.

/**
 * A union containing heading information gathered from sensor data.
 */

union RawHeading {
    int16_t array[3]; ///< Array containing the yaw, pitch and roll as uint16_ts
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t yaw;   ///< The yaw angle as measured by the sensor.
        int16_t roll;  ///< The roll angle as measured by the sensor.
        int16_t pitch; ///< The pitch angle as measured by the sensor.
    };
};

union RawLinearAcceleration {
    int16_t array[3]; ///< Array containing the x-, y- and z-acceleration.
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t x; ///< The linear acceleration in the x direction.
        int16_t y; ///< The linear acceleration in the y direction.
        int16_t z; ///< The linear acceleration in the z direction.
    };
};

union RawAngularVelocity {
    int16_t array[3]; ///< Array containing the x, y and z angular velocity.
    // Packed struct to ensure that no padding is inserted and the array/struct
    // share the exact same memory
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t p; ///< The angular velocity component in the x dir.
        int16_t q; ///< The angular velocity component in the y dir.
        int16_t r; ///< The angular velocity component in the z dir.
    };
};

union RawQuaternion {
    int16_t array[4]; ///< Array containing the x,y,z,w components of the quatrn
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        int16_t x; ///< x component of the quaternion
        int16_t y; ///< y component of the quaternion
        int16_t z; ///< z component of the quaternion
        int16_t w; ///< w component of the quaternion
    };
};

/**
 * @brief The raw pressure output as measured by a pressure sensor.
 */
typedef uint32_t RawPressure;

/**
 * @brief The raw temperature output as measured by a temperature sensor.
 */
typedef int16_t RawTemperature;

/* Physical data ------------------------------------------------------------ */
// This section declares types that represent a physical quantity.

/**
 * @brief The position of the guided recovery system with respect to the
 * desired landing position. The desired landing position is in the origin of
 * the coordinate frame. Coordinates are expressed with respect to the NED
 * frame.
 */
union Position {
    float array[3]; ///< Array containing the positions as floats.
    struct __attribute__((packed)) {
        float north; ///< North position [m]
        float east;  ///< East position [m]
        float down;  ///< Down position [m]
    };
};

/**
 * @brief The position of the system w.r.t. the geodetic coordinate system.
 *
 * This is the position as given by the GPS receiver. It can be converted to a
 * NED position using the conversion functions @see geo2ned.
 */
union GeodeticPosition {
    double array[3]; ///< Array containing the geodetic position as floats
    /** Shortcut struct to access the array members by names. */
    struct __attribute__((packed)) {
        double latitude;  ///< Latitudinal position of the system
        double longitude; ///< Longitudinal position of the system
        double elevation; ///< Altitude above sea level
    };
};

union Velocity {
    float array[3]; ///< Array to access all three members at once
    struct __attribute__((packed)) {
        float u; ///< Velocity along north axis [m/s]
        float v; ///< Velocity along east axis [m/s]
        float w; ///< Velocity along down axis [m/s]
    };
};

/**
 * A union containing heading information gathered from sensor data.
 */
union Heading {
    float array[3]; ///< Array containing the yaw, pitch and roll as floats.
    /** Shortcut struct to access the different array members by names */
    struct __attribute__((packed)) {
        float yaw;   ///< The yaw angle as measured by the sensor [rad].
        float roll;  ///< The roll angle as measured by the sensor [rad].
        float pitch; ///< The pitch angle as measured by the sensor [rad].
    };
};

/**
 * A union containing a linear acceleration vector that describes the linear
 *  acceleration of the system (without the static gravitational acceleration).
 */
union LinearAcceleration {
    float array[3]; ///< Array containing the x-, y- and z-acceleration.
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float x; ///< The linear acceleration in the x direction [m/s^2]
        float y; ///< The linear acceleration in the y direction [m/s^2]
        float z; ///< The linear acceleration in the z direction [m/s^2]
    };
};

/**
 * A union containing an angular velocity vector.
 */
union AngularVelocity {
    float array[3]; ///< Array containing the x, y and z angular velocity.
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float p; ///< The angular velocity component in the x dir [rad/s]
        float q; ///< The angular velocity component in the y dir [rad/s]
        float r; ///< The angular velocity component in the z dir [rad/s]
    };
};

/**
 * A union containing a unit quaternion.
 */
union Quaternion {
    float array[4]; ///< Array containing the x,y,z,w components of the quatrn
    /** Shortcut struct to access the different array members by names. */
    struct __attribute__((packed)) {
        float x; ///< x component of the quaternion
        float y; ///< y component of the quaternion
        float z; ///< z component of the quaternion
        float w; ///< w component of the quaternion
    };
};

typedef float Temperature;
typedef float Pressure;
/** Altitude above ground level */
typedef float AltitudeAGL;

/* Sensor specific data ----------------------------------------------------- */

// This section groups the raw sensor reading types into structs that represent
// a readout of a particular sensor.

/**
 * @brief An enum characterizing which sensor output is contained in a packet.
 */
enum SensorType {
    BNO055_Sensor = 1, ///< The data comes from a BNO-055 9-axis IMU.
    BMP388_Sensor = 2  ///< The data comes from a BMP-388 pressure sensor.
};

/**
 * A struct containing the different readouts of the BNO-055.
 *
 * The BNO-055 9-Axis Sensor will do sensor fusion on its values. The values in
 * this struct are those that get read out in a single sample.
 */
struct BNO055_Readout {
    uint32_t tick;                       ///< Tick when the sample was taken.
    union RawQuaternion qtr_att;         ///< Quaternion describing the attitude
    union RawLinearAcceleration lin_acc; ///< The linear acceleration.
    union RawAngularVelocity ang_vel;    ///< The angular velocity.
};

/**
 * @brief A struct containing the readout of the BMP-388 from the registers.
 *
 * The values are already compensated, but not yet converted to the right units
 * nor floating point.
 */
struct BMP388_Readout {
    RawPressure pressure;       ///< The pressure register entry.
    RawTemperature temperature; ///< The temperature register entry.
};

/**
 * @brief A struct containing the readout of a GPS receiver.
 */
struct NEO7M_Readout {
    uint32_t tick;                   ///< Tick of the measurement
    uint8_t num_satellites;          ///< Number of connected satellites
    union GeodeticPosition position; ///< Position of the GR system
};

/* Control input types ------------------------------------------------------ */
// This section contains the data types that are passed to state estimation and
// control tasks. They are collections of physical quantities (e. g. converted
// sensor data).

/**
 * @brief A struct storing a snapshot in time of all the sensor data available.
 */
struct CompleteSensorData {
    struct BNO055_Readout main_att_readout_1;   ///< BNO055 readout #1 (main)
    struct BNO055_Readout main_att_readout_2;   ///< BNO055 readout #2 (main)
    struct BNO055_Readout icsb_att_readout_1;   ///< BNO055 readout #1 (ICSB)
    struct BNO055_Readout icsb_att_readout_2;   ///< BNO055 readout #2 (ICSB)
    struct BMP388_Readout main_tube_pressure_1; ///< BMP388 readout #1 (main)
    struct BMP388_Readout main_tube_pressure_2; ///< BMP388 readout #2 (main)
    struct BMP388_Readout icsb_pressure_left;   ///< BMP388 readout #1 (ICSB)
    struct BMP388_Readout icsb_pressure_right;  ///< BMP388 readout #2 (ICSB)
    struct NEO7M_Readout geodetic_pos_1;        ///< NEO7m readout #1 (main)
    struct NEO7M_Readout geodetic_pos_2;        ///< NEO7M readout #2 (main)
    /** 16-bit flag indicating which measurements have changed since last
     * deadline. */
    uint16_t changed_readings;
};

// Defines that allow to identify which readings have changed
#define MAIN_ATT_READOUT_1_CHANGED (1 << 0)
#define MAIN_ATT_READOUT_2_CHANGED (1 << 1)
#define ICSB_ATT_READOUT_1_CHANGED (1 << 2)
#define ICSB_ATT_READOUT_2_CHANGED (1 << 3)
#define MAIN_PRS_READOUT_1_CHANGED (1 << 4)
#define MAIN_PRS_READOUT_2_CHANGED (1 << 5)
#define ICSB_PRS_READOUT_1_CHANGED (1 << 6)
#define ICSB_PRS_READOUT_2_CHANGED (1 << 7)
#define MAIN_GPS_READOUT_1_CHANGED (1 << 8)
#define MAIN_GPS_READOUT_2_CHANGED (1 << 9)
#define MAIN_GPS_READOUT_3_CHANGED (1 << 10)

/**
 * @brief Data type representing the system's current state, estimated or real.
 */
struct State {
    union Position position;
    union Velocity velocity;
    union AngularVelocity angular_velocity;
    union Heading attitude;
};

/* Conversion functions ----------------------------------------------------- */

// These functions convert the register readouts to the physical quantities.
// They also give the units to the measurements.

/**
 * Converts the raw heading received from the ICSB to a float-based struct
 * that can be used in calculations.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 */
void convert_raw_heading(union RawHeading *in, union Heading *out);

/**
 * Converts the raw acceleration received from the ICSB to a float-based struct
 * that can be used in calculations.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 */
void convert_raw_acceleration(union RawLinearAcceleration *in,
                              union LinearAcceleration *out);

/**
 * Converts the raw ang. velocity received from the ICSB to a float-based struct
 * that can be used in calculations.
 * @param in The int16_t based reading from the BNO.
 * @param out The float based, converted output.
 */
void convert_raw_angular_velocity(union RawAngularVelocity *in,
                                  union AngularVelocity *out);

/**
 * Converts the raw quaternion read from the IMU to a float-based RPY heading.
 * @param in The raw 16-bit integer quaternion data read from the absolute
 *      orientation sensor.
 * @param out The heading based on the roll-pitch-yaw convention.
 */
void convert_raw_quaternion_to_heading(union RawQuaternion *in,
                                       union Heading *out);

/**
 * Converts the heading to raw quaternions.
 * @param in The heading angles in RPY fashion.
 * @param out The corresponding normalized quaternion.
 */
void convert_heading_to_raw_quaternion(union Heading *in,
                                       union RawQuaternion *out);

/**
 * Converts the raw pressure measurement of a barometric sensor to an altitude
 * above ground level.
 */
AltitudeAGL convert_raw_pressure_to_altitude(RawPressure pressure);
