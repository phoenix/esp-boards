/**
 ******************************************************************************
 * @file           bmp-388.c
 * @brief          Implementation of the BMP-388 pressure sensor.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <assert.h>
#include <stdint.h>

#include "esp_log.h"

#include "bmp-388.h"
#include "bmp3-bosch.h"
#include "bmp3_defs.h"
#include "bmp3_selftest.h"
#include "i2c-bus.h"

/* Private variables -------------------------------------------------------- */

const char *TAG = "bmp-388";

/* Faulty sensor value detection -------------------------------------------- */

// Why dPa here? Because the BMP3 API returns the readings in that format.

/** Lowest pressure reading that is accepted [dPa] */
#define BMP388_PRESSURE_LOWER_BOUND (30000 * 100)
/** Highest pressure reading that is accepted in [dPa] */
#define BMP388_PRESSURE_UPPER_BOUND (110000 * 100)

/* Private functions -------------------------------------------------------- */

/**
 * @brief Wrapper function for the BMP-388 to access the I2C bus.
 *
 * @param reg_addr BMP 388 internal register address
 * @param data Pointer where data should be read from.
 * @param len Data length in bytes
 * @param intf_ptr Pointer to BMP388 handle which is being sampled
 * @return int8_t
 */
int8_t bmp388_bus_write(uint8_t reg_addr, const uint8_t *data, uint32_t len,
                        void *intf_ptr) {
    BMP388_Handle *bmp = (BMP388_Handle *)intf_ptr;
    return i2c_master_write_slave_reg(bmp->bmp388_i2c_addr, reg_addr,
                                      (uint8_t *)data, len, bmp->i2c_port_num);
}

/**
 * @brief Wrapper function for the BMP-388 to access the I2C bus.
 *
 * @param reg_addr BMP 388 internal register address
 * @param data Pointer where read data should be written to.
 * @param len Data length in bytes
 * @param intf_ptr Pointer to BMP388 handle which is being sampled
 * @return int8_t
 */
int8_t bmp388_bus_read(uint8_t reg_addr, uint8_t *data, uint32_t len,
                       void *intf_ptr) {
    BMP388_Handle *bmp = (BMP388_Handle *)intf_ptr;
    return i2c_master_read_slave_reg(bmp->bmp388_i2c_addr, reg_addr, data, len,
                                     bmp->i2c_port_num);
}

/**
 * @brief A wrapper for the BMP-388 API to use microsecond delays.
 *
 * @param[in] period Number of microseconds to delay.
 * @param[in] intf_ptr The interface pointer, which contains the I2C port number
 */
void bmp388_delay_us(uint32_t period, void *intf_ptr) {
    ets_delay_us(period);
}

/**
 * Run the built-in selftest of the BMP-388 sensor.
 * @param bmp_388 A pointer to a non-null BMP388_Handle including the I2C handle
 * @returns True if the selftest succeeded, false otherwise.
 */
bool bmp388_run_selftest(BMP388_Handle *bmp_388);

/* Public function implementation ------------------------------------------- */

bool bmp388_init(BMP388_Handle *bmp_388) {

    ESP_LOGV(TAG, "Initializing BMP-388 with handle at %p", bmp_388);
    if (bmp_388 == NULL) {
        ESP_LOGE(TAG, "Invalid arguments passed to BMP-388 initializer");
        return false;
    }

    // store pointer to the I2C handle in user data - when the API wants to talk
    // to the peripheral, it will provide that pointer
    bmp_388->bmp_internal.intf = BMP3_I2C_INTF;
    bmp_388->bmp_internal.intf_ptr = (void *)bmp_388;

    // the Bosch driver wants function pointers to a microsecond delay function
    // and bus read/write functions
    bmp_388->bmp_internal.delay_us = &bmp388_delay_us;
    bmp_388->bmp_internal.read = &bmp388_bus_read;
    bmp_388->bmp_internal.write = &bmp388_bus_write;

    // Run self-test before setting all our settings. Why? Because this messes
    // up the ODR and everything.
    if (!bmp388_run_selftest(bmp_388)) {
        return false;
    }

    int8_t res = bmp3_init(&(bmp_388->bmp_internal));
    if (res != BMP3_INTF_RET_SUCCESS) {
        ESP_LOGE(TAG, "bmp3_init (Bosch driver) failed with return code (%i)",
                 res);
        return false;
    } else {
        ESP_LOGI(TAG, "BMP-388 init succeeded.");
    }

    // Configure the sensor for: 50 Hz readouts, standard resolution, 8x
    // pressure oversampling, 1x temperature oversampling, IIR filter coeff 2.
    // These settings are recommended for drones in the BMP-388 datasheet, p.57
    bmp_388->bmp_internal.settings = (struct bmp3_settings){
        .press_en = BMP3_ENABLE,
        .temp_en = BMP3_ENABLE,
        .op_mode = BMP3_MODE_NORMAL,
        .odr_filter = {.press_os = BMP3_OVERSAMPLING_8X,
                       .temp_os = BMP3_NO_OVERSAMPLING,
                       .iir_filter = BMP3_IIR_FILTER_COEFF_1,
                       .odr = BMP3_ODR_50_HZ}};

    // mask all settings to be changed and try changing them in one burst write
    if (bmp3_set_sensor_settings(
            BMP3_SEL_PRESS_EN | BMP3_SEL_TEMP_EN | BMP3_SEL_PRESS_OS |
                BMP3_SEL_TEMP_OS | BMP3_SEL_IIR_FILTER | BMP3_SEL_ODR,
            &(bmp_388->bmp_internal)) != BMP3_INTF_RET_SUCCESS) {
        ESP_LOGE(TAG, "Something failed in changing BMP-388 settings!");
        return false;
    } else {
        ESP_LOGI(TAG, "Could successfully set all settings.");
    }

    bmp_388->bmp_internal.settings.op_mode = BMP3_MODE_NORMAL;
    if (bmp3_set_op_mode(&(bmp_388->bmp_internal)) != BMP3_OK) {
        ESP_LOGE(TAG, "Error setting operation mode!");
    }
    return true;
}

bool bmp388_run_selftest(BMP388_Handle *bmp_388) {
    bool success =
        (bmp3_selftest_check(&bmp_388->bmp_internal) == BMP3_SENSOR_OK);

    if (success) {
        ESP_LOGI(TAG, "BMP-388 selftest successful.");
    } else {
        ESP_LOGE(TAG, "BMP-388 self test failed!");
    }

    return success;
}

bool bmp388_sample(BMP388_Handle *bmp_388, struct BMP388_Readout *readout) {
    if (!bmp_388 || !readout) {
        ESP_LOGE(TAG, "Invalid parameters passed to bmp388_sample()");
        return false;
    }

    struct bmp3_data data = {0};
    if (bmp3_get_sensor_data(BMP3_ALL, &data, &(bmp_388->bmp_internal)) ==
        BMP3_INTF_RET_SUCCESS) {

        if (data.pressure > BMP388_PRESSURE_LOWER_BOUND &&
            data.pressure < BMP388_PRESSURE_UPPER_BOUND) {
            readout->temperature =
                (int16_t)data.temperature;               // temperature * 100
            readout->pressure = (uint32_t)data.pressure; // pressure * 100
            ESP_LOGV(TAG, "BMP-388 sample: Pressure: %i Pa, Temperature: %i C",
                     (uint32_t)data.pressure / 100,
                     (int16_t)data.temperature / 100);
            return true;
        } else {
            ESP_LOGE(TAG, "BMP-388 sample: Sensor returned faulty value.");
            return false;
        }
    } else {
        ESP_LOGE(TAG, "Failed to sample BMP-388!");
        return false;
    }
};