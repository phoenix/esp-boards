/**
 ******************************************************************************
 * @file           wireless-transmission.c
 * @brief          Implementation of the on-board wireless transmission.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>

#include "esp_log.h"
#include "esp_netif.h"
#include "esp_now.h"
#include "esp_wifi.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "debug.h"
#include "wireless-transmission.h"

/* Local definitions -------------------------------------------------------- */
#define ESPNOW_WIFI_MODE WIFI_MODE_STA
#define ESPNOW_WIFI_IF   ESP_IF_WIFI_STA

// origin, destination, type, priority and payload size
#define ICSB_HEADER_SIZE 5 * sizeof(uint8_t)

static const char *TAG = "wireless-transmission";

/* Local variables ---------------------------------------------------------- */
struct WirelessTaskConfiguration *config = NULL;

/* Private function declarations -------------------------------------------- */

/**
 * Set up the ESP-NOW communication with the receiving board.
 *
 * This method will first ensure that ESP-NOW is properly deinitialized prior to
 * enabling it. This means, it can be called again to reset the ESP-NOW
 * communication. Must be called AFTER WiFi has been initialized.
 * @param config The configuration passed to the wireless task (or NULL).
 * @returns Non-zero on error.
 */
static int espnow_setup(struct WirelessTaskConfiguration *config);

/**
 * @brief Sets up the WiFi peripheral.
 *
 * This first starts the network interface layer, then sets up the WiFi to store
 * its data in RAM and then returns.
 * @returns Zero on success, non-zero on error.
 */
static int wifi_setup(void);

/**
 * ESP-NOW packet reception callback.
 *
 * This function is called by the ESP-NOW WiFi task (ESP-IDF, non-user built).
 * It adds the received packet to the queue to be processed by our wireless
 * transmission task.
 */
void wireless_packet_received(const uint8_t *mac_addr, const uint8_t *data,
                              int data_len);

/*
 * ESP-NOW packet sent callback.
 *
 * This function is called by the ESP-NOW WiFi task (ESP-IDF, non-user built).
 * It handles sending errors.
 */
void wireless_packet_sent(const uint8_t *mac_addr, esp_now_send_status_t st);

/*
 * Convert a MAC address in the format "ff:ff:ff:ff:ff:ff" to its representation
 * with integers.
 * @param string The string to convert
 * @param dest A pointer to the destination array.
 * @returns Whether the action was successful. Non-zero on error.
 * @pre The destination array must have length >= ESP_NOW_ETH_ALEN
 */
static int mac_addr_from_string(const char *string, uint8_t dest[])
    __attribute((nonnull));

/* Public function implementation ------------------------------------------- */

bool wireless_transmission_setup(struct WirelessTaskConfiguration *arg) {
    ESP_LOGI(TAG, "Entered wireless transmission task.");
    ESP_LOGV(TAG, "Configuring ESP-NOW...");
    config = arg;

    ESP_LOGV(TAG, "Configuring WiFi...");
    if (wifi_setup() != 0) {
        ESP_LOGE(TAG, "Setup of WiFi peripheral failed. Aborting.");
        return false;
    }

    if (espnow_setup(config) != 0) {
        ESP_LOGE(TAG, "Configuring ESP-NOW failed!");
        return false;
    }

    // Register callbacks with the underlying APIs
    if (esp_now_register_recv_cb(&wireless_packet_received) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to register receiving callback for ESP-NOW!");
    }

    if (esp_now_register_send_cb(&wireless_packet_sent) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to register sending callback for ESP-NOW!");
    }

    return true;
}

int transmit_packet_wirelessly(Packet *packet) {
    if (packet == NULL) {
        ESP_LOGE(TAG, "Received a NULL packet to transmit wirelessly.");
        return -1;
    }

    // Get the MAC address of the destination board from the array of locations
    uint8_t mac_addr[ESP_NOW_ETH_ALEN];
    memset(mac_addr, 0, ESP_NOW_ETH_ALEN);

    bool found_dest = false;
    for (uint_fast8_t i = 0; i < config->n_peers; i++) {
        ESP_LOGV(TAG, "Comparing destination %i to packet destination %i",
                 config->dests[i], packet->destination);
        if (config->dests[i] == packet->destination) {
            if (mac_addr_from_string(config->mac_peers[i], mac_addr) != 0) {
                ESP_LOGE(TAG, "Failed to convert \"%s\" to MAC address.",
                         config->mac_peers[i]);
            } else {
                found_dest = true;
                break;
            }
        }
    }

    // Check whether we could find a MAC address for the packet's destination
    if (!found_dest) {
        ESP_LOGD(TAG, "Destination of packet does not have associated MAC.");
        return -1;
    }

    char *buf = malloc(ICSB_HEADER_SIZE + packet->payload_size);
    if (!buf)
        return -1;

    // fill buffer that should be transmitted
    buf[0] = (uint8_t)(packet->origin);
    buf[1] = (uint8_t)(packet->destination);
    buf[2] = (uint8_t)(packet->type);
    buf[3] = (uint8_t)(packet->priority);
    buf[4] = (uint8_t)(packet->payload_size);
    memcpy(buf + 5, packet->payload, packet->payload_size);

    esp_err_t ret = ESP_OK;
    if ((ret = esp_now_send(mac_addr, (const uint8_t *)buf,
                            ICSB_HEADER_SIZE + packet->payload_size)) !=
        ESP_OK) {
        ESP_LOGE(TAG, "Failed to send packet to MAC " MACSTR "! Reason: (%i)",
                 MAC2STR(mac_addr), ret);
        return -1;
    }
    ESP_LOGD(TAG, "Sent packet! Cleaning up memory...");
    free(packet->payload);
    free(buf);

    return 0;
}

/* Private function implementation ------------------------------------------ */

static int wifi_setup() {
    esp_err_t ret = ESP_OK;

    // Set up Network IF layer. Not sure why this is necessary, but following
    // example code from Espressif here.
    if ((ret = esp_netif_init()) != ESP_OK) {
        ESP_LOGE(TAG, "Could not set up Wifi! Reason: (%i)", ret);
        return ret;
    }
    esp_event_loop_create_default();
    // Start from valid default config
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    cfg.nvs_enable = 0; // TODO: consider doing that
    if ((ret = esp_wifi_init(&cfg)) != ESP_OK) {
        ESP_LOGE(TAG, "Could not initialize WiFi! Reason: (%i)", ret);
    }

    // Configure for our application: Store data in RAM, run ESP-NOW. Start.
    esp_wifi_set_storage(WIFI_STORAGE_RAM);
    esp_wifi_set_mode(ESPNOW_WIFI_MODE);
    if ((ret = esp_wifi_start()) != ESP_OK) {
        ESP_LOGE(TAG, "WiFi could not be started. Reason: (%i)", ret);
        return ret;
    }

    return ret;
}

static int espnow_setup(struct WirelessTaskConfiguration *config) {

    esp_err_t ret = ESP_OK;
    if ((ret = esp_now_init()) != ESP_OK) {
        ESP_LOGE(TAG, "Could not init ESP-NOW. Reason: (%i)", ret);
        return -1;
    }

    if (config != NULL) {
        ESP_LOGV(TAG, "Trying to add %u peers.", config->n_peers);

        esp_now_peer_info_t peer = {0};
        peer.channel = CONFIG_ESPNOW_CHANNEL; // Change this in the menuconfig
        peer.encrypt = false;                 // Might implement later
        peer.ifidx = ESPNOW_WIFI_IF;          // This is #define'd at the top

        for (uint8_t i = 0; i < config->n_peers; i++) {

            // convert MAC from string to bytes and check validity
            if (mac_addr_from_string(config->mac_peers[i], peer.peer_addr) ==
                0) {
                if ((ret = esp_now_add_peer(&peer) == ESP_OK)) {
                    ESP_LOGI(TAG, "Successfully added peer %u with MAC \"%s\"",
                             i, config->mac_peers[i]);
                }
            } else {
                // design decision: if a MAC address is invalid, don't abort,
                // instead print an error message and go to the next one (if
                // applicable)
                ESP_LOGE(TAG, "Invalid MAC address \"%s\" for peer %u",
                         config->mac_peers[i], i);
                continue;
            }
        }
    }

    return 0;
}

void wireless_packet_received(const uint8_t *mac_addr, const uint8_t *data,
                              int data_len) {
    ESP_LOGV(TAG, "Packet (%i bytes) received from " MACSTR ".", data_len,
             MAC2STR(mac_addr));

    // This callback runs on the ESP WiFi task. It should be kept as short as
    // possible.

    // Construct wireless packet struct. Don't need to allocate memory as
    // sending it to the RTOS queue will then copy all memory, so this instance
    // can safely go out of scope.
    Packet packet = {0};

    // fill metadata
    if (data_len < ICSB_HEADER_SIZE) {
        ESP_LOGI(TAG,
                 "The received packet's size is invalid (header too small)!");
        return;
    }

    packet.origin = data[0];
    packet.destination = data[1];
    packet.type = data[2];
    packet.priority = data[3];
    packet.payload_size = data[4];
    packet.payload = malloc(packet.payload_size);

    // need to copy the payload data though, because after going out of scope of
    // this function it could be deleted
    if (!packet.payload && packet.payload_size > 0) {
        ESP_LOGE(TAG,
                 "No memory could be allocated for the received packet!\n"
                 "Reason: %s",
                 strerror(errno));
        return;
    }

    memcpy(packet.payload, data + ICSB_HEADER_SIZE, packet.payload_size);
    if (packet_add_to_queue(&packet) != 0) {
        // queue probably full, release memory and drop
        free(packet.payload);
        ESP_LOGE(TAG, "Could not put packet into queue. Dropped packet.");
    }

    return;
}

void wireless_packet_sent(const uint8_t *mac_addr, esp_now_send_status_t st) {
    if (st != ESP_NOW_SEND_SUCCESS) {
        ESP_LOGE(TAG, "Failed to send packet to MAC address " MACSTR "!",
                 MAC2STR(mac_addr));
    }
}

static int mac_addr_from_string(const char *string, uint8_t dest[]) {
    if ((sscanf(string, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c", &(dest[0]),
                &(dest[1]), &(dest[2]), &(dest[3]), &(dest[4]), &(dest[5]))) ==
        6)
        return 0;
    else
        return 1;
}
