/**
 *******************************************************************************
 * @file           wireless-transmission.h
 * @brief          Declaration of the wireless transmission methods
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020-2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#ifndef __WIRELESS_TRANSMISSION_H__
#define __WIRELESS_TRANSMISSION_H__

#include <stdbool.h>

#include "esp_now.h"
#include "packets.h"

/**
 * The struct containing the configuration for the wireless transmission task.
 *
 * The reason for this struct's existence is code portability: The goal is that
 * both the receiver board and the ICSB can share the same transmission code to
 * ensure that both run the same version and bugs get fixed in both instances.
 */
struct WirelessTaskConfiguration {
    uint8_t n_peers; ///< Number of peers for this wireless node
    const char *
        *mac_peers; ///< Array containing the MACs for the specified peers
    const enum Location
        *dests; ///< Array containing the destinations for the MACs
};

/**
 * Set up the wireless transmission chain.
 * @param arg The configuration to be passed to the wireless transmission chain.
 * @returns Whether the setup of the wireless module was successful.
 */
bool wireless_transmission_setup(struct WirelessTaskConfiguration *config)
    __attribute__((nonnull));

/**
 * Transmit a packet wirelessly via ESP-NOW.
 *
 * These packets are normally received from the packet dispatch task out of the
 * send/receive queue. This function will accept the packet and send it out by
 * handing it over to the low-level WiFi task.
 * @param packet The packet to send out.
 * @returns Nothing because the packet is sent async w.r.t. the function call.
 * @pre The origin of the packet must be 'Local'.
 * @pre The packet must be nonnull.
 * @pre The queue must have been created.
 */
int transmit_packet_wirelessly(Packet *packet);

#endif /* __WIRELESS_TRANSMISSION_H__ */
