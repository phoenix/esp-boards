/**
 *******************************************************************************
 * @file           : console.c
 * @brief          : Interactive console presented on serial port
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <string.h>

#include "argtable3/argtable3.h"
#include "driver/uart.h"
#include "esp_console.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_vfs.h"
#include "esp_vfs_dev.h"
#include "linenoise/linenoise.h"

#include "main.h"
#include "wireless-console.h"

/** Struct that contains the argtable for the "icsb_cmd" command */
static struct {
    struct arg_int *cmd_num;      ///< The integer command that should be sent
    struct arg_str *target_board; ///< String "left", "right" or "both"
    struct arg_end *end;          ///< End of the argtable
} cmd_args;

/* Private functions -------------------------------------------------------- */

/**
 * @brief Sends a command packet with the command as payload to an ICSB.
 * @param argc Number of passed arguments from esp_console
 * @param argv Arguments passed from esp_console
 * @returns zero on success, non-zero otherwise.
 */
int icsb_cmd(int argc, char **argv);

/* Public function implementation ------------------------------------------- */

void console_init(void) {
    // This implementation is taken from the console example of ESP-IDF

    /* Drain stdout before reconfiguring it */
    fflush(stdout);
    fsync(fileno(stdout));

    /* Disable buffering on stdin */
    setvbuf(stdin, NULL, _IONBF, 0);

    /* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
    esp_vfs_dev_uart_port_set_rx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM,
                                              ESP_LINE_ENDINGS_CR);
    /* Move the caret to the beginning of the next line on '\n' */
    esp_vfs_dev_uart_port_set_tx_line_endings(CONFIG_ESP_CONSOLE_UART_NUM,
                                              ESP_LINE_ENDINGS_CRLF);

    /* Configure UART. Note that REF_TICK is used so that the baud rate remains
     * correct while APB frequency is changing in light sleep mode. */
    const uart_config_t uart_config = {
        .baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
#if CONFIG_IDF_TARGET_ESP32 || CONFIG_IDF_TARGET_ESP32S2
        .source_clk = UART_SCLK_REF_TICK,
#else
        .source_clk = UART_SCLK_XTAL,
#endif
    };
    /* Install UART driver for interrupt-driven reads and writes */
    ESP_ERROR_CHECK(
        uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM, 256, 0, 0, NULL, 0));
    ESP_ERROR_CHECK(
        uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config));

    /* Tell VFS to use UART driver */
    esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);

    /* Initialize the console */
    esp_console_config_t console_config = {
        .max_cmdline_args = 8,
        .max_cmdline_length = 256,
#if CONFIG_LOG_COLORS
        .hint_color = atoi(LOG_COLOR_CYAN)
#endif
    };
    ESP_ERROR_CHECK(esp_console_init(&console_config));

    /* Configure linenoise line completion library */
    /* Enable multiline editing. If not set, long commands will scroll within
     * single line.
     */
    linenoiseSetMultiLine(1);

    /* Tell linenoise where to get command completions and hints */
    linenoiseSetCompletionCallback(&esp_console_get_completion);
    linenoiseSetHintsCallback((linenoiseHintsCallback *)&esp_console_get_hint);

    /* Set command history size */
    linenoiseHistorySetMaxLen(100);

    /* Don't return empty lines */
    linenoiseAllowEmpty(false);

    // Register commands
    esp_console_cmd_t slp_cmd = {
        .command = "slp",
        .help = "Calculates the sea level pressure given the current "
                "environment pressure",
        .hint = NULL,
        .func = take_pressure_measurement,
        .argtable = NULL,
    };

    cmd_args.cmd_num = arg_int0(NULL, "num", "<int>", "Command number");
    cmd_args.target_board =
        arg_str1(NULL, NULL, "<board>", "\"left\", \"right\" or \"both\"");
    cmd_args.end = arg_end(2);

    esp_console_cmd_t icsb_send_cmd = {
        .command = "icsb_cmd",
        .help = "Send a command to the ICSBs.",
        .hint = NULL,
        .func = icsb_cmd,
        .argtable = &cmd_args,
    };

    esp_console_cmd_t icsb_wake = {
        .command ="icsb_wake",
        .help = "Wake up the ICSBs.",
        .hint = NULL,
        .func = icsb_wakeup,
        .argtable = NULL,
    };

    esp_console_cmd_register(&slp_cmd);
    esp_console_cmd_register(&icsb_send_cmd);
    esp_console_cmd_register(&icsb_wake);

    esp_console_register_help_command();
}

char *console_get_line(void) {
    /* Get a line using linenoise.
     * The line is returned when ENTER is pressed. */
    char *line = linenoise(LOG_COLOR_I "wireless-module> " LOG_RESET_COLOR);
    if (line == NULL) { /* Break on EOF or error */
        return NULL;
    }
    /* Add the command to the history if not empty*/
    if (strlen(line) > 0) {
        linenoiseHistoryAdd(line);
    }
    return line;
}

bool console_run_command(char *line) {
    bool success = false;
    int ret;
    esp_err_t err = esp_console_run(line, &ret);
    if (err == ESP_ERR_NOT_FOUND) {
        printf("Unrecognized command\n");
        success = false;
    } else if (err == ESP_ERR_INVALID_ARG) {
        // command was empty
        success = false;
    } else if (err == ESP_OK && ret != ESP_OK) {
        printf("Command returned non-zero error code: 0x%x (%s)\n", ret,
               esp_err_to_name(ret));
        success = false;
    } else if (err != ESP_OK) {
        printf("Internal error: %s\n", esp_err_to_name(err));
        success = false;
    } else {
        success = true;
    }
    /* linenoise allocates line buffer on the heap, so need to free it */
    linenoiseFree(line);

    return success;
}

/* Private functions -------------------------------------------------------- */

int icsb_cmd(int argc, char **argv) {

    int nerrors = arg_parse(argc, argv, (void **)&cmd_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, cmd_args.end, argv[0]);
        return 1;
    }

    uint8_t cmd_num = cmd_args.cmd_num->ival[0];
    const char *str = cmd_args.target_board->sval[0];

    bool icsb_left = false;
    bool icsb_right = false;

    if (strncmp(str, "left", 5) == 0) {
        icsb_left = true;
    } else if (strncmp(str, "right", 5) == 0) {
        icsb_right = true;
    } else if (strncmp(str, "both", 4) == 0) {
        icsb_left = true;
        icsb_right = true;
    }

    return icsb_send_command(cmd_num, icsb_left, icsb_right);
}