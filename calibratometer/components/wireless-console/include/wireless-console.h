/**
 *******************************************************************************
 * @file           : console.h
 * @brief          : Interactive console presented on serial port
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ARIS under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#pragma once

/**
 * @brief Initializes the serial console.
 */
void console_init(void);

/**
 * @brief Gets a line from the user via serial port.
 * @returns The line as a string buffer.
 */
char *console_get_line(void);

/**
 * @brief Runs the command in the line as a console command.
 * @returns Whether running the command was successful
 */
bool console_run_command(char *line);
