/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Definitions for the main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#pragma once

#include <stdbool.h>

#include "packets.h"

/* Task management ---------------------------------------------------------- */

#define TASK_MAIN_PRIORITY 5 ///< Priority of the main program body (FSM)
#define TASK_SENS_PRIORITY 4 ///< Priority of the sensor polling task
#define TASK_PCKT_PRIORITY 3 ///< Priority of the WiFi transmission task
#define TASK_BLNK_PRIORITY 2 ///< Priority of the LED blinking task.
#define TASK_STAT_PRIORITY 2 ///< Priority of the status sending task

/* Error handling ----------------------------------------------------------- */

/**
 * Handler for when a fatal error occurred in the system. Delay forever.
 */
void fatal_error(void) __attribute__((noreturn));

/* Public functions --------------------------------------------------------- */

/**
 * @brief Takes a pressure measurement and prints the corresponding sea level
 * pressure for known locations.
 * @param argc Number of passed arguments from esp_console
 * @param argv Arguments passed from esp_console
 * @returns Zero on success, non-zero otherwise.
 */
int take_pressure_measurement(int argc, char **argv);

/**
 * @brief Sends a command to the ICSB.
 * @param cmd_num Number of the command to send.
 * @param icsb_left Whether to send it to the left ICSB.
 * @param icsb_right Whether to send it to the right ICSB.
 * @note icsb_left and icsb_right can both be set to true. If both are set, the
 * command is sent to both.
 */
int icsb_send_command(uint8_t cmd_num, bool icsb_left, bool icsb_right);

/**
 * @brief Starts the ICSB wake-up routine.
 */
int icsb_wakeup(int argc, char **argv);

/**
 * Packet reception handler for the in-canopy sensor board.
 *
 * This reception handler is called by the packet handling task upon receiving
 * a packet addressed to this microcontroller. It runs on the computational time
 * of the wireless task. This is fine, because the in-canopy boards don't often
 * receive remote commands. In addition, these commands (active / sleep) are not
 * milli-second critical.
 * @param packet The packet that was received.
 */
void packet_received(Packet *packet);
