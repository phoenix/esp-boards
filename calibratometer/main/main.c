/**
 *******************************************************************************
 * @file           : main.c
 * @brief          : Contains the entire application.
 *******************************************************************************
 * @attention
 *
 * Copyright (c) 2020 Akademische Raumfahrt Initiative Schweiz
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *******************************************************************************
 */

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "driver/uart.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_vfs.h"
#include "esp_vfs_dev.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "bmp-388.h"
#include "i2c-bus.h"
#include "main.h"
#include "packets.h"
#include "wireless-console.h"
#include "wireless-transmission.h"

#define I2C_NUM_0            (0)
#define CONFIG_I2C_0_SCL_PIN (22)
#define CONFIG_I2C_0_SDA_PIN (23)

#define HEIGHT_IPZ  (435.0f)
#define SPL_WICHLEN (1308.6f)

/* Logging ------------------------------------------------------------------ */

static const char *TAG = "calibratometer";
volatile bool left_woken = true;
volatile bool right_woken = true;


struct BMP388_Readout readout;
BMP388_Handle bmp388 = {
    .bmp388_i2c_addr = 0x77,
    .i2c_port_num = 0,
};

/* Main program ------------------------------------------------------------- */

/** Handle to the sensor setup/sampling task. Used to pause/resume sampling. */
static TaskHandle_t packet_task; ///< Handle to the packet handler task.

/**
 * @brief Entry function, called by the underlying ESP-IDF toolchain.
 */
void app_main(void) {

    ESP_LOGI(TAG, "\n"
                  "========================================\n"
                  "   Calibratometer                       \n"
                  "       by Project PHOENIX of ARIS       \n"
                  "========================================\n");

    /* I2C Bus setup -------------------------------------------------------- */

    ESP_LOGV(TAG, "Configuring I2C peripheral...");
    if (!i2c_bus_setup(I2C_NUM_0, CONFIG_I2C_0_SCL_PIN, CONFIG_I2C_0_SDA_PIN)) {
        ESP_LOGE(TAG, "Setup of I2C peripheral failed.");
    }

    if (!bmp388_init(&bmp388)) {
        ESP_LOGE(TAG, "Failed to set up BMP-388!");
        assert(false);
    } else {
        ESP_LOGI(TAG, "BMP-388 set up.");
    }

    /* Configuring packets task --------------------------------------------- */

    // Transmission handler for sending wireless packets to receiver board
    struct PacketTransmissionHandler wifi_icsb_left_handler = {
        .destination = InCanopyBoardLeft,
        .transmitter = &transmit_packet_wirelessly,
    };

    struct PacketTransmissionHandler wifi_icsb_right_handler = {
        .destination = InCanopyBoardRight,
        .transmitter = &transmit_packet_wirelessly,
    };

    // Transmission handler for sending wireless packets through receiver board
    struct PacketTransmissionHandler wifi_recv_board_handler = {
        .destination = ReceiverBoard,
        .transmitter = &transmit_packet_wirelessly,
    };

    struct PacketTransmissionHandler tx_handlers[] = {
        wifi_icsb_left_handler,
        wifi_icsb_right_handler,
        wifi_recv_board_handler,
    };

    // Config for the packet handler: This is the ICSB
    struct PacketTaskConfig packet_config = {
        .here = WirelessRemoteBoard,
        .num_transmission_handlers = 3,
        .tx_handlers = tx_handlers,
        .reception_handler = &packet_received,
    };

    if (xTaskCreate(&packet_task_entry, // entry function
                    "packets",          // descriptive name
                    3000,               // stack size
                    &packet_config,     // argument to pass
                    TASK_PCKT_PRIORITY, // priority
                    &packet_task) == pdPASS) {
        ESP_LOGI(TAG, "Wireless transmission task started.");
    } else {
        ESP_LOGE(TAG,
                 "Creation of packet task didn't return pdPASS. Aborting.");
        fatal_error();
    }

    // Configure the ESP-NOW layer with the destinations that it can relay
    // packets to. If the receiver board serves as a relay, give its MAC address
    // even for packets for the main controller board. The reason is that the
    // wireless task has to send out the packet to the receiver board's MAC even
    // if it should be sent to the mainboard.
    const char *receiver_macs[] = {
        CONFIG_ESPNOW_RECEIVER_MAC,
        CONFIG_ESPNOW_LEFT_ICSB_MAC,
        CONFIG_ESPNOW_RIGHT_ICSB_MAC,
    };

    const enum Location dests[] = {
        ReceiverBoard,
        InCanopyBoardLeft,
        InCanopyBoardRight,
    };

    struct WirelessTaskConfiguration config = {
        .n_peers = 3,
        .mac_peers = receiver_macs,
        .dests = dests,
    };

    if (!wireless_transmission_setup(&config)) {
        ESP_LOGE(TAG, "Setup of ESP-NOW layer has failed. Aborting.");
        fatal_error();
    }

    /* Set up for debug console --------------------------------------------- */

    console_init();

    while (true) {

        char *line = console_get_line();
        if (line)
            console_run_command(line);

        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    esp_restart();
}

void packet_received(Packet *packet) {
    // Light up LED during processing

    // Only react to commands and keep-alives, because no data nor log packets
    // should be sent up to the canopy
    ESP_LOGV(TAG, "Packet received.");
    if (packet->type == LogPacket || packet->type == DataPacket) {
        goto cleanup;
    }

    if (packet->type == CommandPacket) {
        // do nothing
    }

    // respond to keep-alive
    if (packet->type == KeepAlive) {
        Packet response = {0};
        response.destination = packet->origin;
        response.origin = WirelessRemoteBoard;
        response.payload_size = 0;
        response.payload = NULL;
        response.type = KeepAlive;

        packet_add_to_queue(&response);
    }

    if (packet->type == StatePacket) {
        if (packet->payload && *(uint8_t *)packet->payload == 0x03) {
            // ICSB now active
            if (packet->origin == InCanopyBoardLeft)
                left_woken = true;
            if (packet->origin == InCanopyBoardRight)
                right_woken = true;
        }
    }

cleanup:
#if DEBUG == 1
    // keep shiny LED on for 1/100 sec to make it visible
    vTaskDelay(10 / portTICK_PERIOD_MS);
#endif
    free(packet->payload);
    return;
}

int take_pressure_measurement(int argc, char **argv) {
    // get 10 samples of sea level pressure
    float sum = 0;
    uint8_t num = 0;

    for (uint_fast8_t i = 0; i < 10; i++) {
        if (bmp388_sample(&bmp388, &readout)) {
            // Current pressure in Pa
            sum += readout.pressure / 100.0f;
            num++;
            vTaskDelay(20);
        } else {
            ESP_LOGE(TAG, "Could not sample BMP-388!");
        }
    }

    if (num != 0) {
        float pressure = sum / num;
        float slp_wichlen =
            pressure / powf(1 - 0.0065f * SPL_WICHLEN / 288.15f, 5.255f);
        float slp_ipz =
            pressure / powf(1 - 0.0065f * HEIGHT_IPZ / 288.15f, 5.255f);

        ESP_LOGI(TAG, "Sea level pressure for Wichlen,\t%6.f m \t%10.2f Pa",
                 SPL_WICHLEN, slp_wichlen);
        ESP_LOGI(TAG, "Sea level pressure for IPZ,\t\t%6.f m \t%10.2f Pa",
                 HEIGHT_IPZ, slp_ipz);
        ESP_LOGI(TAG, "--- --- --- --- --- --- --- --- --- --- --- --- ---");
    }

    return 0;
}

int icsb_send_command(uint8_t cmd_num, bool icsb_left, bool icsb_right) {
    Packet packet = {0};
    packet.origin = ReceiverBoard;
    packet.type = CommandPacket;
    packet.priority = PriorityNormal;
    packet.payload_size = 1;

    if (icsb_left) {
        packet.payload = malloc(sizeof(uint8_t) * 1);
        *(uint8_t *)packet.payload = cmd_num;
        packet.destination = InCanopyBoardLeft;
        packet_add_to_queue(&packet);
    }
    if (icsb_right) {
        packet.payload = malloc(sizeof(uint8_t) * 1);
        *(uint8_t *)packet.payload = cmd_num;
        packet.destination = InCanopyBoardRight;
        packet_add_to_queue(&packet);
    }
    return 0;
}

int icsb_wakeup(int argc, char **argv) {

    Packet wakeup_packet = {
        .origin = ReceiverBoard,
        .type = CommandPacket,
        .priority = 1,
        .payload_size = 1,
    };

    left_woken = false;
    right_woken = false;

    while (true) {

        // Remember the memory invariant: Allocation for payload
        // happens BEFORE adding the packet to the queue and the
        // transmission handler frees it

        if (!left_woken) {
            // Left ICSB
            wakeup_packet.destination = InCanopyBoardLeft;
            wakeup_packet.payload = malloc(1);
            *((uint8_t *)wakeup_packet.payload) = CommandActivate;
            packet_add_to_queue(&wakeup_packet);
        }

        if (!right_woken) {
            // Right ICSB
            wakeup_packet.destination = InCanopyBoardRight;
            wakeup_packet.payload = malloc(1);
            *((uint8_t *)wakeup_packet.payload) = CommandActivate;
            packet_add_to_queue(&wakeup_packet);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }

        if (left_woken && right_woken) {
            ESP_LOGI(TAG, "Both ICSBs are awake!");
            return 0;
        }
    }
}

void fatal_error(void) {
    ESP_LOGE(TAG, "========================\n"
                  "===== FATAL ERROR ======\n"
                  "========================\n");

    while (true) {
        vTaskDelay(portMAX_DELAY);
    }
}
